<?php
/**
 * Created by PhpStorm.
 * User: sulaco-tec
 * Date: 10/23/19
 * Time: 10:11 PM
 */

require_once __DIR__ . '/vendor/autoload.php';

include __DIR__ . '/app/config/config.php';

// For Ip2Location
use Hive\Lib\Ip2Location\Ip2Location;
$ip = new Ip2Location();
$result = $ip->getCountryCodeByIpAddress('16779263');

// For Redis Call
use Hive\Lib\Redis\Redis;
$redis = new Redis();
$redis->set('test', json_encode(array('a',b)));

$result = $redis->get('test');

print_r(json_decode($result));