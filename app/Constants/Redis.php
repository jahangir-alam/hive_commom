<?php
/**
 * Created by PhpStorm.
 * User: sulaco-tec
 * Date: 10/23/19
 * Time: 9:16 PM
 */

namespace Hive\Constants;


interface Redis
{
    /** @var string  */
    const HOST = '127.0.0.1';

    /** @var string  */
    const PORT = '6379';

}