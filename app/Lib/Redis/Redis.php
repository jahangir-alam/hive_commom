<?php
/**
 * Created Md. Jahangir Alam<jahangir@sulaco-tec.com>
 * User: sulaco-tec
 * Date: 10/19/19
 * Time: 1:51 PM
 */

namespace Hive\Lib\Redis;

use Hive\Constants\Redis AS RedisConstants;

class Redis extends \Redis {

    /**
     * Redis constructor.
     * @param string $host
     * @param string $port
     */
    public function __construct($host = RedisConstants::HOST, $port = RedisConstants::PORT)
    {
        $this->connect($host, $port);

    }

}