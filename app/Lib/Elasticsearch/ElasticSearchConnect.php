<?php

namespace Hive\ElasticSearch;

use Elasticsearch\ClientBuilder;

abstract class ElasticSearchConnect
{
    protected $hosts = [];

    protected $index;

    protected $type;

    protected $bulkCount = 0;

    protected $dateElement = 'dateTime';

    protected $params = [];

    protected $sendAction;

    protected $client;

    const UNDEFINED_INDEX_VALUE = 'undefined_index';
    const UNDEFINED_TYPE_VALUE = 'undefined_type';
    const MAX_QUEUED_INDEXES = 1000;

    public function __construct($hosts, $sendAction = 'index', $indexProperties = [])
    {
        $this->hosts = $hosts;
        $this->sendAction = $sendAction;

        $this->client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

        if ($sendAction != 'instance_only') {
            if (!$this->client->indices()->exists(['index' => $this->index])) {
                $indexProperties[$this->dateElement] = [
                    'type' => 'date'
                ];
                $this->createIndexAndMappings($indexProperties);
            }
        }
    }

    /**Ads data array according to sendAction param, chunks data
     *
     * @param $id
     * @param array $data
     */
    public function addToBulkSend($id, array $data)
    {
        $this->bulkCount += 1;
        switch ($this->sendAction) {
            case 'index':
                $this->addToBulkIndex($id, $data);
                break;
            case 'update':
                $this->addToBulkUpdate($id, $data);
                break;
        }

        if ($this->bulkCount % ElasticSearchConnect::MAX_QUEUED_INDEXES == 0) {
            $this->send();
        }
    }

    /**Ads new data to be indexed, needs unique ID
     *
     * @param $id
     * @param array $data
     */
    public function addToBulkIndex($id, array $data)
    {
        $this->params["body"][] = [
            'index' => [
                '_index' => $this->index ?? ElasticSearchConnect::UNDEFINED_INDEX_VALUE,
                //'_type' => $this->type ?? ElasticSearchConnect::UNDEFINED_TYPE_VALUE,
                '_id' => $id,
            ]
        ];

        /*if ($this->dateElement != 0) {
            $this->params["body"][] = [
                'mappings' => [
                    $this->type => [
                    'properties' => [
                        $this->dateElement => [
                            'type' => 'date'
                            ]
                        ]
                    ]
                ]
            ];
        }*/

        if ($this->dateElement) {
            $data[$this->dateElement] = date("c", time());
        }

        $this->params["body"][] = $data;
    }

    /**Ads data to be updated, needs existing id
     *
     * @param $id
     * @param array $data
     */
    public function addToBulkUpdate($id, array $data)
    {
        $this->params["body"][] = [
            "update" => [
                "_index" => $this->index ?? ElasticSearchConnect::UNDEFINED_INDEX_VALUE,
                //'_type' => $this->type ?? ElasticSearchConnect::UNDEFINED_TYPE_VALUE,
                '_id'    => $id
            ]
        ];

        $this->params["body"][] = ['doc' => $data];
    }

    /**Sends bulk of data to be written on ES
     *
     * @return array
     */
    public function send()
    {
        if (isset($this->params["body"]) and $this->params["body"] != []) {
            $response = $this->client->bulk($this->params);
            $this->params["body"] = [];
            return $response;
        }

        return 0;
    }

    /**
     * @param $indexProperties
     * @return array|callable
     */
    public function createIndexAndMappings($indexProperties)
    {
        $params = [
            'index' => $this->index,
            'body' => [
                'mappings' => [
                    'properties' => $indexProperties
                ]
            ]
        ];

        return $this->client->indices()->create($params);
    }

}
