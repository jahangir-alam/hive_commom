<?php

namespace Hive\ElasticSearch;

require_once('vendor/autoload.php');

class PhiveESRequest extends ElasticSearchConnect
{
    protected $index = 'phive_requests';
    protected $type = 'requests';

    protected $dateElement = 'dateTime';

    public function __construct($hosts, $sendAction = 'index')
    {
        parent::__construct($hosts, $sendAction);
    }
}
