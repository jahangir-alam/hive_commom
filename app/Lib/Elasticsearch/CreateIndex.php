<?php

namespace Hive\ElasticSearch;

require_once('vendor/autoload.php');

class CreateIndex extends ElasticSearchConnect
{
    public function __construct($hosts, $indexName, $sendAction = 'index', $indexProperties = [])
    {
        $this->index = $indexName;
        parent::__construct($hosts, $sendAction, $indexProperties);
    }
}
