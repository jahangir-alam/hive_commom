<?php

namespace Hive\ElasticSearch;

use Hive\Constants\ElasticSearch;

require_once('vendor/autoload.php');

class PhiveESClick extends ElasticSearchConnect
{
    protected $index = 'phive_clicks';
    protected $type = 'clicks';

    protected $dateElement = 'dateTime';

    public function __construct($sendAction = 'index')
    {
        $hosts = ElasticSearch::HOSTS;

        parent::__construct($hosts, $sendAction);
    }
}
