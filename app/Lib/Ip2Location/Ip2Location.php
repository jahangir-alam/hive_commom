<?php
/**
 * Created by Md. Jahangir Alam < jahangir@sulaco-tec.com>.
 * User: sulaco-tec
 * Date: 10/19/19
 * Time: 4:29 PM
 */

namespace Hive\Lib\Ip2Location;

use Hive\Config\APP;
use Hive\Lib\Common\DbConnection;

class Ip2Location
{
    /**
     * Extend DbConnection Trait
     */
    use DbConnection;


    /**
     * Get Country Code by Ip Address
     * @param $ipAddress
     * @return string
     */
    public function getCountryCodeByIpAddress($ipAddress)
    {
        /**
         * @var string
         */
        $countryCode = '';

        // Filter IP Address
        $ipAddress = str_replace('.', '', $ipAddress);

        // Get Database Path
        $dbPath = HIVE_ROOT . 'storage/database/ip2location.db';

        if ($this->setConnection($dbPath) instanceof self && !is_null($this->connection)) {
            try {
                $selectSql = "SELECT country_code FROM ip2location_db1 WHERE :ip_address BETWEEN ip_from AND ip_to;";
                $sth = $this->connection->prepare($selectSql);
                $sth->execute([':ip_address' => $ipAddress]);
                $countryCode = $sth->fetchColumn();
                if ($countryCode == '-') {
                    $countryCode = "";
                }
            } catch (\Exception $ex) {
                error_log($ex->getMessage());
            }
        }

        return $countryCode;
    }
}
