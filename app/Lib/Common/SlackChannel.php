<?php
/**
 * Created by Md. Jahangir Alam <jahangir@sulaco-tec.com>.
 * User: sulaco-tec
 * Date: 10/19/19
 * Time: 5:22 PM
 */

namespace Hive\Lib\Common;

use Hive\Helper\Helper;

/**
 * Send Message in Slack Channel
 * Trait SlackChannel
 * @package Hive\Traits
 */
trait SlackChannel
{
    /**
     * Send Message in Slack Channel
     * @param $message string|array
     * @return boolean
     */
    protected function sendMessage($message)
    {
        if (!$message) {
            return false;
        }

        try {

            $helper = new Helper();
            $helper->notifyDeveloperUsingSlack($message);

        } catch (\Exception $ex) {

            error_log($ex->getMessage());
            return false;

        }

        return true;
    }
}