<?php
/**
 * Created by Md. Jahangir Alam <jahangir@sulaco-tec.com>.
 * User: sulaco-tec
 * Date: 10/19/19
 * Time: 4:57 PM
 */

namespace Hive\Lib\Common;

trait DbConnection
{
    /**
     * Extend SlackChannel Trait
     */
    use SlackChannel;

    /**
     * @var string|null
     */
    protected $connection = null;

    /**
     * @var string
     */
    protected $dbPath;

    /**
     * @return string
     */
    protected function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param string $dbPath
     * @return $this|boolean
     * @throws \InvalidArgumentException
     */
    protected function setConnection($dbPath)
    {
        if ($this->dbPath == $dbPath && !is_null($this->connection)) {
            return $this;
        }

        if (!file_exists($dbPath)) {

            $this->sendMessage([
                "Source: " . __TRAIT__,
                "Function: setConnection()",
                "``` Ip2Location database not exist ```"
            ]);

            return false;
        }

        try {

            $this->connection = new \PDO(sprintf('sqlite:%s', $dbPath));

        } catch (\PDOException $e) {
            $this->sendMessage([
                "Source: " . __TRAIT__,
                "Function: setConnection()",
                "``` {$e->getMessage()} ```"
            ]);

            return false;

        }

        return $this;
    }
}
