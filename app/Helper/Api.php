<?php

namespace Hive\Helper;

class Api
{

    /**
     * @var \Hive\Helper\Helper
     */
    protected $helper;

    /**
     * @var mixed = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();

        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = $this->helper->getConfig();
    }

    /**
     * Get shop list by making API call.
     *
     * Get all shops by api calling usually takes 2 seconds.
     *
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $blackListedShops = Array
        (
            [1019] => 1019
            [1021] => 1021
        )
     * @param $minNumberOfOffers = 10
     * @param $cpc = 0.10
     * @return array = Array
        (
            [16091] => Array
                (
                    [shop_id] => 16091
                    [shop_name] => all-4-baby.de
                    [shop_url] => https://www.all-4-baby.de/
                    [cpc] => 0.215
                    [total_hits] => 1607
                )
            ...
     * @throws \Exception
     */
    public function getShopListFromDirectApi($apiConfig, $minNumberOfOffers, $cpc, $blackListedShops)
    {
        try {

            $shops = null;

            /**
             * billiger.de gives shop list through API call
             * s24.com gives shop list through FTP
             */
            switch ($apiConfig['api_name']) {
                case 's24':
                    $s24Helper = new S24\S24Helper();
                    $shops = $s24Helper->makeS24Shops($apiConfig);
                    break;
                case 'solutenetwork':
                    //$shops = $this->makeSoluteNetworkShops($api); // nothing to for the time being as we are not using solute here.
                    break;
                default:
                    // All billiger api
                    $billigerHelper = new Billiger\BilligerHelper();
                    $shops = $billigerHelper->makeBilligerShops($apiConfig, $minNumberOfOffers, $cpc, $blackListedShops);
            }

            return $shops;
        } catch (\Exception $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: /hive.noctemque.com/traffic-includes/class/Api.php",
                "Function: getShopListFromDirectApi()",
                "``` {$e->getMessage()} ```"
            ]);
            echo 'Api Connection failed: ' . $e->getMessage();
            exit;
        }
    }
}
