<?php

namespace Hive\Helper;

class Domain
{
    /**
     * @var \Hive\Helper\Helper
     */
    protected $helper;

    /**
     * @var mixed = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();

        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = $this->helper->getConfig();
    }

    /**
     * Get Shops by highest cpc and domain name.
     *
     * @param $domainName = 'ellerbrockshop.de'
     * @param $portalName = 'discountheld_prod_api'
     * @return mixed = Array
        (
            [0] => Array
                (
                    [portal_title] => Preispudel_prod_API
                    [0] => Preispudel_prod_API
                    [shop_id] => 10108
                    [1] => 10108
                    [max_cpc] => 0.26
                    [2] => 0.26
                )
        )
     */
    public function getShopByDomainPortalName($domainName, $portalName = false)
    {
        try {
            $dBConnection = new DBConnection();
            $shopListDBConnection = $dBConnection->shopListDBConnection();

            $additionalQuery = '';

            if ($portalName) {
                $additionalQuery = "AND a.portal_title = '$portalName'";
            }

            $sql = "SELECT
                        a.portal_title,
                        b.shop_id,
                        b.company_url,
                        MAX(b.cpc) as max_cpc
                    FROM
                        portal_entity AS a
                    INNER JOIN shop_entity AS b ON a.id = b.portal_entity_id
                    WHERE
                        b.company_url LIKE '%$domainName%'
                        AND b.total_hits >= '10'
                        $additionalQuery
                    ORDER BY
                        b.shop_entity_id ASC";

            $results = $shopListDBConnection->prepare($sql);
            $results->execute();
            return iterator_to_array($results);
        } catch (\Exception $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: traffic-includes/class/Domain.php",
                "Function: getShopByDomainPortalName()",
                "``` {$e->getMessage()} ```"
            ]);
            return false;
        }
    }

    /**
     * @param $portalTitle = 'discountheld_prod_API'
     * @return mixed = 'https://discountheld.de/s/red.php?id={shop_id}'
     */
    public function getAClickUrl($portalTitle)
    {
        return isset($this->config['api'][$portalTitle]['a_click_url']) ? $this->config['api'][$portalTitle]['a_click_url'] : false;
    }

    /**
     * Insert data into = 'hub.noctemque.com/data/best-of-shopping.net/db/click_bid_log.db' >> 'log_click_detail'
     *
     * CREATE TABLE `log_click_detail` (
            `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            `portal_title`	TEXT DEFAULT NULL,
            `request_detail`	TEXT NOT NULL,
            `response_detail`	TEXT,
            `unique_id`	TEXT,
            `active_status`	integer,
            `created_at`	TEXT,
            `updated_at`	TEXT
    );
     * @param $data = Array
        (
            [portal_title] => discountheld_prod_API
            [request_detail] => /get_cpc.php?d=123reifen.de&api=discountheld_prod_API
            [response_detail] => {"bid":"0.22","link":"https:\/\/discountheld.de\/s\/red_d_b.php?d=123reifen.de"}
            [unique_id] => 5c62bcb03ac87
            [active_status] => 1
            [created_at] => 2019-02-12 12:31:44
            [updated_at] => 2019-02-12 12:31:44
        )
     * @return mixed = false | 7
     */
    public function insertClickBid($data)
    {
        try {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            $dBConnection = new DBConnection();
            $clickBidDBConnection = $dBConnection->clickBidDBConnection();
            return $this->helper->flatInsert($clickBidDBConnection, 'log_click_detail', $data);

        } catch (\Exception $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: traffic-includes/class/Domain.php",
                "Function: insertClickBid()",
                "``` {$e->getMessage()} ```"
            ]);
            return false;
        } catch (\PDOException $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: traffic-includes/class/Domain.php",
                "Function: insertClickBid()",
                "``` {$e->getMessage()} ```"
            ]);
            return false;
        }
    }
}
