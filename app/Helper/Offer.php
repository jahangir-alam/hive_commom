<?php

namespace Hive\Helper;

use Hive\Helper\Billiger\BilligerHelper;
use Hive\Helper\S24\S24Helper;
use Hive\Helper\Connexity\ConnexityHelper;

class Offer
{
    /**
     * Max values to generate random weighted offers.
     */
    const MAX = [
        1 => '900',
        2 => '1718',
        3 => '2523',
        4 => '3261',
        5 => '3937',
        6 => '4496',
        7 => '5004',
        8 => '5466',
        9 => '5886',
        10 => '6268',
        11 => '6615',
        12 => '6930',
        13 => '7217',
        14 => '7478',
        15 => '7715',
        16 => '7930',
        17 => '8126',
        18 => '8304',
        19 => '8466',
        20 => '8613',
        21 => '8747',
        22 => '8869',
        23 => '8980',
        24 => '9081',
        25 => '9172',
        26 => '9255',
        27 => '9331',
        28 => '9400',
        29 => '9462',
        30 => '9519',
        31 => '9571',
        32 => '9618',
        33 => '9661',
        34 => '9700',
        35 => '9735',
        36 => '9767',
        37 => '9796',
        38 => '9822',
        39 => '9846',
        40 => '9868',
        41 => '9888',
        42 => '9906',
        43 => '9922',
        44 => '9937',
        45 => '9951',
        46 => '9963',
        47 => '9974',
        48 => '9984',
        49 => '9993',
        50 => '10001',
    ];

    /**
     * @var \Hive\Helper\Helper
     */
    protected $helper;

    /**
     * @var mixed = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();

        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = $this->helper->getConfig();
    }

    /**
     * Get random weighted offer from cache.
     *
     * When cache file not exist or cache file does not contain offer, get offer from direct api call.
     *
     * @param $portal = 'turbopreise'
     * @param $shopId = '15558'
     * @return mixed = Array
        (
            [shop] => Natura Vitalis
            [price_per_unit] => 397,60 € / 100 ml
            [availability_code] => green
            [brand_id] => 1741796
            ...
        )
     */
    public function getOffer($portal, $shopId)
    {
        $contents = [];

        /**
         * @var $apiConfig = Array
            (
                [api_base] => api.billiger.de/content/2.0
                [api_user] => turbopreise_prod_API
                [api_pw] => *****
                [mc] => Kjqy8tnekP63
                [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
                [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
            )
         */
        $apiConfig = isset($this->config['api'][$portal]) ? $this->config['api'][$portal] : false;

        if (!$apiConfig) {
            return $contents;
        }

        /**
         * @var $apiName = 'billiger' | 's24' | 'solutenetwork'
         */
        $apiName = $apiConfig['api_name'];

        try {
            /**
             * @var $cacheDir = 'hive.noctemque.com/traffic-includes/../../turbopreise/html/offers_cache'
             */
            $cacheDir = isset($apiConfig['offers_cache']) ? $apiConfig['offers_cache'] : false;

            if ($cacheDir) {
                /**
                 * @var $cacheFile = 'turbopreise/html/offers_cache/15558.json'
                 */
                $cacheFile = $this->config['rootDir'] . "/" .$cacheDir . "/$shopId.json";

                /**
                 * When cache file exist get weighted offer from that file
                 */
                if (file_exists($cacheFile)) {
                    $contents = file_get_contents($cacheFile);

                    /**
                     * @var $contents = Array
                        (
                            [hits] => Array
                                (
                                    [0] => Array
                                        (
                                            [shop] => Natura Vitalis
                                            [price_per_unit] => 379,35 € / 100 ml
                                            [availability_code] => green
                                            ...
                                        )
                                    ...
                     */
                    $contents = json_decode($contents, true);
                } else {
                    $this->helper->notifyDeveloperUsingSlack([
                        "Source: traffic-includes/class/Offer.php",
                        "Function: getOffer()",
                        "Portal: {$apiConfig['api_user']}",
                        "``` '$cacheFile' not exist! ```"
                    ]);
                    if ($apiName == 'connexity') {
                        $this->helper->notifyDeveloperUsingSlack([
                            "Source: traffic-includes/class/Offer.php",
                            "Function: getOffer()",
                            "Portal: {$apiConfig['api_user']}",
                            "``` '$cacheFile' not exist! ```",
                            "``` Message: Connexity do not return offer only with shop id,
                             it requires cat id and keyword
                             Wee make cache form feed for connexity in hub.
                             If cache file not exit we have nothing to do here. ```"
                        ]);

                        /**
                         * Connexity do not return offer only with shop id, it requires cat id and keyword
                         * Wee make cache form feed for connexity in hub.
                         * If cache file not exit we have nothing to do here.
                         */
                        exit;
                    }
                }
            }

            /**
             * When cache file not exist or cache file does not have any offer ('turbopreise/html/offers_cache/15558.json')
             */
            if (($apiName == 'billiger' && (!isset($contents['hits']) || !count($contents['hits'])))
                || ($apiName == 's24' && (!isset($contents['products']) || !count($contents['products'])))
                || ($apiName == 'connexity' && (!isset($contents[0]) || !count($contents[0])))
                || ($apiName == 'publisher.kelkoo.com' && (!isset($contents['Products']['Product']) || !count($contents['Products']['Product'])))
            ) {
                /**
                 * billiger.de gives shop list through API call
                 * s24.com gives shop list through FTP
                 */
                switch ($apiName) {
                    case 's24':
                        $contents = $this->getOfferFromS24APi($shopId, $apiConfig, 100);
                        break;
                    case 'solutenetwork':
                        // nothing to for the time being as we are not using solute here.
                        break;
                    case 'publisher.kelkoo.com':
                        /**
                         * @var $url = 'http://fr.shoppingapis.kelkoo.com/V3/productSearch?logicalType=and&results=100&start=1&show_subcategories=0&show_refinements=0&show_products=1&merchantId=21369&aid=96958398×tamp=1565086638&hash=jaR7NkVRuPIh2ZcM3sH0rw--'
                         */
                        $searchUrl = $this->helper->kelkooUrlSigner("http://de.shoppingapis.kelkoo.com", "/V3/productSearch?logicalType=and&results=100&start=1&show_subcategories=0&show_refinements=0&show_products=1&merchantId=$shopId", $apiConfig['api_user'], $apiConfig['api_pw']);
                        $contents = $this->helper->getKelkooUrl($searchUrl);
                        break;
                    default:
                        // All billiger api
                        $contents = $this->getOfferFromBilligerAPi($shopId, $apiConfig, 100);
                        /**
                         * @var $contents = Array
                            (
                            [hits] => Array
                                (
                                [0] => Array
                                    (
                                        [shop] => Natura Vitalis
                                        [price_per_unit] => 379,35 € / 100 ml
                                        [availability_code] => green
                                        ...
                                    )
                                ...
                        */
                        $contents = json_decode($contents, true);
                }
            }

            $offers = null;

            /**
             * billiger.de gives shop list through API call
             * s24.com gives shop list through FTP
             */
            switch ($apiName) {
                case 's24':
                    $offers = isset($contents['products']) ? $contents['products'] : null;
                    break;
                case 'connexity':
                    $offers = $contents;
                    break;
                case 'solutenetwork':
                    // nothing to for the time being as we are not using solute here.
                    break;
                case 'publisher.kelkoo.com':
                    $offers = isset($contents['Products']['Product']) ? $contents['Products']['Product'] : null;
                    break;
                default:
                    // All billiger api
                    $offers = isset($contents['hits']) ? $contents['hits'] : null;
            }

            if (isset($offers) && count($offers)) {
                /**
                 * Currently \Hive\Offer::getWeightedOffer() function assumes that, max total offer is 50.
                 */
                $totalOffer = count($offers);

//                if (isset($_GET['debug']) && $_GET['debug']) {
//                    echo "<br>\$totalOffer = $totalOffer <br>";
//                }

                /**
                 * Skype: 2019-02-20
                 *
                 * if we have 150 offers, we use current implementation ands extend this by 1
                    so as if we have 51 offers
                    then we let old implementation select from this
                    if offer number 51 is selected, we select normal straight random from the remaining 100
                    hard to explain, but I hope you understand
                    so, first 50 offer will follow the current implementation
                    > 50 all are the same, so no weight or probability, just random selection of the 100
                 */
                $i = $this->getWeightedOfferForBigCollection($totalOffer);

//                if (isset($_GET['debug']) && $_GET['debug']) {
//                    echo "<br>getting offer using hive.noctemque.com/traffic-includes/class/Offer.phpr::getWeightedOfferForBigCollection('new implementation')<br>";
//                    echo "<br>Current Offer Number is $i<br><br>";
//                }

                return $offers[$i - 1];
            }

            return false;
        } catch (\Exception $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: traffic-includes/class/Offer.php",
                "Function: getOffer()",
                "Portal: {$apiConfig['api_user']}",
                "``` {$e->getMessage()} ```"
            ]);
            exit;
        }
    }

    /**
     * When cache file not exist or offer file doesnot contain offer, get offers from direct api call.
     *
     * @param $shopId = 15558
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $pageSize = 50
     * @return mixed
     */
    public function getOfferFromBilligerAPi($shopId, $apiConfig, $pageSize)
    {
        $params = [
            'filter:shop' => $shopId,
            'doctype' => 'offer,product_offer',
            'filter_flags' => 'off',
            'sort' => 'clickout_relevance',
            'page_size' => $pageSize,
            'page' => 0,
        ];

        $apiObj = new BilligerHelper();
        return $apiObj->apiCall('search', $params, $apiConfig);
    }

    /**
     * When cache file not exist or offer file doesnot contain offer, get offers from direct api call.
     *
     * @param $shopId = 15558
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $pageSize = 50
     * @return mixed
     */
    public function getOfferFromS24APi($shopId, $apiConfig, $pageSize)
    {
        $apiObj = new S24Helper();
        return $apiObj->getS24Offers($apiConfig, $shopId, $pageSize);
    }

    /**
     * When cache file not exist or offer file doesnot contain offer, get offers from direct api call.
     *
     * @param $shopId = 15558
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $pageSize = 50
     * @return mixed
     */
    public function getOfferFromConnexityAPi($shopId, $apiConfig, $pageSize)
    {
        $apiObj = new ConnexityHelper();
        return $apiObj->getConnexityOffers($apiConfig, $shopId, $pageSize);
    }

    /**
     * Generates random weighted array index based on total offer count.
     *
     * @param $totalOffers = 100
     * @return int = 46
     */
    public function getWeightedOffer($totalOffers)
    {
        /**
         * Total offer must be within 50
         */
        $totalOffers = 50 < $totalOffers ? 50 : $totalOffers;

        $maxRand = ($totalOffers == 0) ? 1 : self::MAX[$totalOffers];

        $r = mt_rand(1, $maxRand);
        $i = 0;

        if (($r <= 900)) {
            $i = 1;
        } elseif ((900 < $r) && ($r <= 1718)) {
            $i = 2;
        } elseif ((1718 < $r) && ($r <= 2523)) {
            $i = 3;
        } elseif ((2523 < $r) && ($r <= 3261)) {
            $i = 4;
        } elseif ((3261 < $r) && ($r <= 3937)) {
            $i = 5;
        } elseif ((3937 < $r) && ($r <= 4496)) {
            $i = 6;
        } elseif ((4496 < $r) && ($r <= 5004)) {
            $i = 7;
        } elseif ((5004 < $r) && ($r <= 5466)) {
            $i = 8;
        } elseif ((5466 < $r) && ($r <= 5886)) {
            $i = 9;
        } elseif ((5886 < $r) && ($r <= 6268)) {
            $i = 10;
        } elseif ((6268 < $r) && ($r <= 6615)) {
            $i = 11;
        } elseif ((6615 < $r) && ($r <= 6930)) {
            $i = 12;
        } elseif ((6930 < $r) && ($r <= 7217)) {
            $i = 13;
        } elseif ((7217 < $r) && ($r <= 7478)) {
            $i = 14;
        } elseif ((7478 < $r) && ($r <= 7715)) {
            $i = 15;
        } elseif ((7715 < $r) && ($r <= 7930)) {
            $i = 16;
        } elseif ((7930 < $r) && ($r <= 8126)) {
            $i = 17;
        } elseif ((8126 < $r) && ($r <= 8304)) {
            $i = 18;
        } elseif ((8304 < $r) && ($r <= 8466)) {
            $i = 19;
        } elseif ((8466 < $r) && ($r <= 8613)) {
            $i = 20;
        } elseif ((8613 < $r) && ($r <= 8747)) {
            $i = 21;
        } elseif ((8747 < $r) && ($r <= 8869)) {
            $i = 22;
        } elseif ((8869 < $r) && ($r <= 8980)) {
            $i = 23;
        } elseif ((8980 < $r) && ($r <= 9081)) {
            $i = 24;
        } elseif ((9081 < $r) && ($r <= 9172)) {
            $i = 25;
        } elseif ((9172 < $r) && ($r <= 9255)) {
            $i = 26;
        } elseif ((9255 < $r) && ($r <= 9331)) {
            $i = 27;
        } elseif ((9331 < $r) && ($r <= 9400)) {
            $i = 28;
        } elseif ((9400 < $r) && ($r <= 9462)) {
            $i = 29;
        } elseif ((9462 < $r) && ($r <= 9519)) {
            $i = 30;
        } elseif ((9519 < $r) && ($r <= 9571)) {
            $i = 31;
        } elseif ((9571 < $r) && ($r <= 9618)) {
            $i = 32;
        } elseif ((9618 < $r) && ($r <= 9661)) {
            $i = 33;
        } elseif ((9661 < $r) && ($r <= 9700)) {
            $i = 34;
        } elseif ((9700 < $r) && ($r <= 9735)) {
            $i = 35;
        } elseif ((9735 < $r) && ($r <= 9767)) {
            $i = 36;
        } elseif ((9767 < $r) && ($r <= 9796)) {
            $i = 37;
        } elseif ((9796 < $r) && ($r <= 9822)) {
            $i = 38;
        } elseif ((9822 < $r) && ($r <= 9846)) {
            $i = 39;
        } elseif ((9846 < $r) && ($r <= 9868)) {
            $i = 40;
        } elseif ((9868 < $r) && ($r <= 9888)) {
            $i = 41;
        } elseif ((9888 < $r) && ($r <= 9906)) {
            $i = 42;
        } elseif ((9906 < $r) && ($r <= 9922)) {
            $i = 43;
        } elseif ((9922 < $r) && ($r <= 9937)) {
            $i = 44;
        } elseif ((9937 < $r) && ($r <= 9951)) {
            $i = 45;
        } elseif ((9951 < $r) && ($r <= 9963)) {
            $i = 46;
        } elseif ((9963 < $r) && ($r <= 9974)) {
            $i = 47;
        } elseif ((9974 < $r) && ($r <= 9984)) {
            $i = 48;
        } elseif ((9984 < $r) && ($r <= 9993)) {
            $i = 49;
        } elseif ((9993 < $r) && ($r <= 10001)) {
            $i = 50;
        }

        return $i;
    }

    /**
     * Skype: 2019-02-20
     *
     * if we have 150 offers, we use current implementation ands extend this by 1
    so as if we have 51 offers
    then we let old implementation select from this
    if offer number 51 is selected, we select normal straight random from the remaining 100
    hard to explain, but I hope you understand
    so, first 50 offer will follow the current implementation
    > 50 all are the same, so no weight or probability, just random selection of the 100
     *
     * @param $totalOffers
     * @return bool|int|string
     */
    public function getWeightedOfferForBigCollection($totalOffers)
    {
        $weightedValues = [
            '1' => 89990000000000000,
            '2' => 81808181818181800,
            '3' => 80514620544825100,
            '4' => 73752787336710900,
            '5' => 67605666238425200,
            '6' => 55872919075323900,
            '7' => 50792653704839900,
            '8' => 46174230640763600,
            '9' => 41975664218876000,
            '10' => 38158785653523600,
            '11' => 34688896048657800,
            '12' => 31534450953325300,
            '13' => 28666773593932100,
            '14' => 26059794176301900,
            '15' => 23689812887547200,
            '16' => 21535284443224700,
            '17' => 19576622221113400,
            '18' => 17796020201012100,
            '19' => 16177291091829200,
            '20' => 14705719174390200,
            '21' => 13367926522172900,
            '22' => 12151751383793600,
            '23' => 11046137621630500,
            '24' => 10041034201482300,
            '25' => 9127303819529340,
            '26' => 8296639835935760,
            '27' => 7541490759941600,
            '28' => 6854991599946910,
            '29' => 6230901454497190,
            '30' => 5663546776815630,
            '31' => 5147769797105110,
            '32' => 4678881633731920,
            '33' => 4252619667029020,
            '34' => 3865108788208200,
            '35' => 3512826171098360,
            '36' => 3192569246453060,
            '37' => 2901426587684600,
            '38' => 2636751443349630,
            '39' => 2396137675772390,
            '40' => 2177397887065810,
            '41' => 1978543533696190,
            '42' => 1797766848814720,
            '43' => 1633424408013380,
            '44' => 1484022189103070,
            '45' => 1348201990093700,
            '46' => 1224729081903370,
            '47' => 1112480983548520,
            '48' => 1010437257771380,
            '49' => 917670234337616,
            '50' => 833336576670560,
            '> 50' => 500000000000000,
        ];

        if ($totalOffers <= 50) {
            /**
             * reduce array to return key within max '$totalOffers' limit.
             */
            $weightedValues = array_slice($weightedValues, 0, $totalOffers, true);

            return $this->getRandomWeightedElement($weightedValues);
        } else {
            /**
             * For testing only
             */
            if (isset($_GET['debug']) && $_GET['debug'] && isset($_GET['test_big']) && $_GET['test_big']) {
                return mt_rand(51, $totalOffers);
            }

            $itemNumber = $this->getRandomWeightedElement($weightedValues);
            
            if ($itemNumber && $itemNumber != '> 50') {
                return $itemNumber;
            } else {
                return mt_rand(51, $totalOffers);
            }
        }
    }

    /**
     * Utility function for getting random values with weighting.
     * Pass in an associative array, such as array('A'=>5, 'B'=>45, 'C'=>50)
     * An array like this means that "A" has a 5% chance of being selected, "B" 45%, and "C" 50%.
     * The return value is the array key, A, B, or C in this case.  Note that the values assigned
     * do not have to be percentages.  The values are simply relative to each other.  If one value
     * weight was 2, and the other weight of 1, the value with the weight of 2 has about a 66%
     * chance of being selected.  Also note that weights should be integers.
     *
     * @param array $weightedValues
     * @return bool|int|string
     */
    public function getRandomWeightedElement(array $weightedValues)
    {
        $rand = mt_rand(1, (int) array_sum($weightedValues));

        foreach ($weightedValues as $key => $value) {
            $rand -= $value;
            if ($rand <= 0) {
                return $key;
            }
        }

        return false;
    }
}
