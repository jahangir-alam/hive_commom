<?php

namespace Hive\Helper;

class ShopList
{
    /**
     * @var \Hive\Helper\Helper
     */
    protected $helper;

    /**
     * @var mixed = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();

        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = $this->helper->getConfig();
    }

    /**
     * Get active shop list by specific portal
     *
     * Global Filter for all: current_status, total_hit, cpc, blacklist
     *
     * Fallback Step - 1: Get shop list from pre-generated json: '/hub.noctemque.com/data/shoplist/toppreistipps_prod_API_shoplist.json' [we have .xml format too into the same directory]
     * Fallback Step - 2: If the pre generated json does not exist then get all active shops from 'hub.noctemque.com/data/db/shoplist_clone.db'
     * Fallback Step - 3: When 'hub.noctemque.com/data/db/shoplist_clone.db' also gets empty result try making direct API call to get shops.[it will be little bit slow process]
     *
     * Note: We generate and maintain all active shops, cpc and total hit in 'hub.noctemque.com/data/db/shoplist_clone.db' by
     * cron job which runs every 5 minutes.
     * We also generates 'data/shoplist/toppreistipps_prod_API_shoplist.xml' file with that cron.
     *
     * Cron Location: 'http://hub.noctemque.com/get-offer/add-total-hit-by-portal?portal=discountheld_prod_API'
     * Please add the controller and function name ??
     *
     * @param $portal = 'turbopreise'
     * @param $minNumberOfOffers = 10
     * @param $cpc = 0.10
     * @param $clickType = 'a' | 'b' | '0'
     * @return array = Array
        (
            [10025] => 10025
            [10092] => 10092
            [10108] => 10108
            [10111] => 10111
            ...
     */
    public function getLiveShops($portal, $minNumberOfOffers = 10, $cpc = 0.10, $clickType = null)
    {
        try {
            /**
             * @var $apiConfig = Array
                (
                    [api_base] => api.billiger.de/content/2.0
                    [api_user] => turbopreise_prod_API
                    [api_pw] => *****
                    [mc] => Kjqy8tnekP63
                    [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
                    [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
                )
             */
            $apiConfig = $this->config['api'][$portal];

            /**
             * @var $availableShops = Array
                (
                    [10025] => Array
                        (
                            [shop_id] => 10025
                            [shop_name] => Ellerbrock-shop.de
                            [shop_url] => https://www.ellerbrockshop.de/
                            [cpc] => 0.26
                            [total_hits] => 1989
                        )
                    ...
             */
            $availableShops = $this->getShopListFromJsonFile($apiConfig, $minNumberOfOffers, $cpc);

            if (isset($availableShops) && is_array($availableShops) && count($availableShops)) {
                return $availableShops;
            } else {
                $this->helper->notifyDeveloperUsingSlack([
                    "Source: /hive.noctemque.com/traffic-includes/class/ShopList.php",
                    "Function: getLiveShops()",
                    "Portal: {$apiConfig['user_key']}",
                    "``` Important: Json Shop List file not exist in HUB! ```"
                ]);

                /**
                 * When shops not found in 'hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json' file.
                 * Second alternate direct db query
                 */
                $availableShops = $this->getShopListFromDatabase($apiConfig, $minNumberOfOffers, $cpc);
            }

            if (isset($availableShops) && is_array($availableShops) && count($availableShops)) {
                return $availableShops;
            } else {
                $this->helper->notifyDeveloperUsingSlack([
                    "Source: /hive.noctemque.com/traffic-includes/class/ShopList.php",
                    "Function: getLiveShops()",
                    "Portal: {$apiConfig['user_key']}",
                    "``` Important: Shop List data not found in HUB shoplist_clone.db! ```"
                ]);

                /**
                 * @var $blackListedShops = Array
                    (
                        [1019] => 1019
                        [1021] => 1021
                    )
                 */
                $blackListedShops = $this->getBlackListedShops($apiConfig, $clickType);

                /**
                 * When shops not found in 'hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json' file.
                 * When shops not found in 'hub.noctemque.com/data/db/shoplist_clone.db' >> 'shop_entity' table.
                 * third alternate api call
                 */
                $apiObj = new Api();
                $availableShops = $apiObj->getShopListFromDirectApi($apiConfig, $minNumberOfOffers, $cpc, $blackListedShops);

                /**
                 * Just try to check if still we need to process this situation.
                 */
                $this->helper->notifyDeveloperUsingSlack([
                    "URL: http://hive.noctemque.com/shoplist.php",
                    "Source: /hive.noctemque.com/traffic-includes/class/ShopList.php",
                    "Function: getLiveShops()",
                    "Portal: {$apiConfig['user_key']}",
                    "```Need to process direct API call for shop list. NO data found into both .json and .db```"
                ]);
            }

            if (isset($availableShops) && is_array($availableShops) && count($availableShops)) {
                return $availableShops;
            } else {
                /**
                 * All above 3 steps got no data.
                 */
                $this->helper->notifyDeveloperUsingSlack([
                    "Source: /hive.noctemque.com/traffic-includes/class/ShopList.php",
                    "Function: getLiveShops()",
                    "Portal: {$apiConfig['user_key']}",
                    "```No Shop found with 3 step process!```"
                ]);
            }

            return $availableShops;
        } catch (\Exception $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: /hive.noctemque.com/traffic-includes/class/ShopList.php",
                "Function: getLiveShops()",
                "``` {$e->getMessage()} ```"
            ]);
            exit;
        }
    }

    /**
     * Get black listed shop from 'hub.noctemque.com/data/db/shoplist_clone.db' >> 'blacklisted_shop_id'
     * Black list can be filtered by 'a' or 'b' click.
     * We basically add, edit and delete black listed shop from here 'http://hub.noctemque.com/blacklist'
     *
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $clickType = 'a' | 'b' | '0'
     * @return array = Array
        (
            [1019] => 1019
            [1021] => 1021
            ...
        )
     */
    public function getBlackListedShops($apiConfig, $clickType)
    {
        $blackListedShops = [];

        $portal = isset($apiConfig['api_user']) ? $apiConfig['api_user'] : '';
        $dBConnection = new DBConnection();
        $shopListDBConnection = $dBConnection->shopListDBConnection();

        $additionalQuery = "";

        if ($clickType != null) {
            $additionalQuery = "AND b.click_type = '$clickType'";
        }

        $sql= "SELECT
                    shop_id
                FROM
                    portal_entity AS a
                INNER JOIN blacklisted_shop_id AS b ON
                    a.id = b.portal_entity_id
                WHERE
                    a.portal_title = :portal
                    $additionalQuery
                ORDER BY
                    b.shop_id";

        $results = $shopListDBConnection->prepare($sql);
        $results->bindParam(':portal', $portal, \PDO::PARAM_STR);
        $results->execute();

        /**
         * Example data per row. Here we select only shop_id
         *
         * @var $result = Array
            (
                [id] => 212
                [0] => 7
                [portal_title] => turbopreise_prod_API
                [1] => turbopreise_prod_API
                [portal_detail] =>
                [2] =>
                [created_at] => 2019-01-25 12:16:22
                [3] => 2018-07-16 09:20:02
                [updated_at] => 2019-01-25 12:16:22
                [4] => 2018-07-16 09:20:02
                [5] => 212
                [portal_entity_id] => 7
                [6] => 7
                [click_type] => a
                [7] => a
                [company_url] =>
                [8] =>
                [shop_id] => 111
                [9] => 111
                [detail_comment] =>
                [10] =>
                [11] => 2019-01-25 12:16:22
                [12] => 2019-01-25 12:16:22
            )
         */
        foreach ($results as $result) {
            $blackListedShops[$result['shop_id']] = $result['shop_id'];
        }

        return $blackListedShops;
    }

    /**
     * Get pre generated portal wise shoplist from 'hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json'
     *
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $minNumberOfOffers = 10
     * @param $cpc = 0.10
     * @return array = Array
        (
            [10025] => Array
                (
                    [shop_id] => 10025
                    [shop_name] => Ellerbrock-shop.de
                    [shop_url] => https://www.ellerbrockshop.de/
                    [cpc] => 0.26
                    [total_hits] => 1989
                )
            ...
     */
    public function getShopListFromJsonFile($apiConfig, $minNumberOfOffers, $cpc)
    {
        /**
         * variable initialization as an array
         */
        $availableShops = [];

        /**
         * Pre-generated .json file through cron.
         *
         * @var $shopListPath = 'hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json'
         */
        $shopListPath = $this->config['rootDir'] .'/'. $apiConfig['shop_list_path'];

        if (file_exists($shopListPath)) {
            /**
             * File exist, we need to generate live shop ids from 'hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json'
             */
            $content = file_get_contents($shopListPath);

            /**
             * @var $shopsArray = Array
                (
                    [0] => Array
                        (
                            [shop_id] => 10025
                            [shop_name] => Ellerbrock-shop.de
                            [shop_url] => https://www.ellerbrockshop.de/
                            [$cpc] => 0.26
                            [total_hits] => 2008
                        )
                ...
             */
            $shopsArray = json_decode($content, true);

            /**
             * @var $shop = Array
                (
                    [shop_id] => 10025
                    [shop_name] => Ellerbrock-shop.de
                    [shop_url] => https://www.ellerbrockshop.de/
                    [cpc] => 0.26
                    [total_hits] => 2008
                )
             */
            foreach ($shopsArray as $shop) {
                if ($shop['total_hits'] < $minNumberOfOffers || $shop['cpc'] < $cpc) {
                    continue;
                }

                $availableShops[$shop['shop_id']] = [
                    'shop_id' => isset($shop['shop_id']) ? $shop['shop_id'] : '',
                    'shop_name' => isset($shop['shop_name']) ? $shop['shop_name'] : '',
                    'domain' => isset($shop['domain']) ? $shop['domain'] : '',
                    'shop_url' => $this->helper->fixShopUrl(isset($shop['shop_url']) ? $shop['shop_url'] : ''),
                    'cpc' => isset($shop['cpc']) ? $shop['cpc'] : '',
                    'total_hits' => isset($shop['cpc']) ? $shop['total_hits'] : '',
                ];
            }
        }

        return $availableShops;
    }

    /**
     * This function is used in 'top-preistipps.de/html/es/ProcessTrafficData.php',
     * 'preiskenner.de/html/es/ProcessTrafficData.php' to get click related information like domain, cpc_get of a click
     *
     * @param $apiUser = 'toppreistipps_prod_API'
     * @param $minNumberOfOffers = 1
     * @param $cpc = 0.01
     * @return array = Array
        (
            [0] => Array
                (
                    [shop_entity_id] => 9548
                    [api_entity_id] => 1
                    [portal_entity_id] => 4
                    [shop_id] => 10025
                    [shop_name] => Ellerbrock-shop.de
                    [company_url] => https://ellerbrockshop.de/
                    [domain_name_only] => ellerbrockshop.de
                    [total_hits] => 1957
                    [last_update_total_hit] => 2019-10-15 09:01:06
                    [current_status] => 1
                    [cpc] => 0.26
                    [reference_product_name] => Siemens KI24DA30 Einbau-Kühl-Gefrierautomat
                    [reference_product_offer_id] => 948104158
                    [created_at] => 2018-07-19 13:55:26
                    [updated_at] => 2018-07-19 13:55:26
                )
     */
    public function getAllShopsFromDb($apiUser, $minNumberOfOffers, $cpc)
    {
        $dBConnection = new DBConnection();

        /**
         * Connect with '/hub.noctemque.com/data/db/shoplist_clone.db'
         */
        $shopListDBConnection = $dBConnection->shopListDBConnection();

        $sql = "SELECT
                  b.*
                FROM
                    portal_entity AS a
                INNER JOIN shop_entity AS b ON a.id = b.portal_entity_id
                WHERE
                    a.portal_title = :portal
                    -- AND b.current_status = 1
                    AND b.total_hits >= :total_hits
                    AND b.cpc >= :cpc
                ORDER BY
                    b.shop_id";

        $results = $shopListDBConnection->prepare($sql);
        $results->bindParam(':portal', $apiUser, \PDO::PARAM_STR);
        $results->bindParam(':total_hits', $minNumberOfOffers, \PDO::PARAM_STR);
        $results->bindParam(':cpc', $cpc, \PDO::PARAM_STR);
        $results->execute();
        return $results->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [user_key] => turbopreise_prod_API
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $minNumberOfOffers = 10
     * @param $cpc = 0.10
     * @return array = Array
        (
            [10025] => Array
                (
                    [shop_id] => 10025
                    [shop_name] => Ellerbrock-shop.de
                    [shop_url] => https://www.ellerbrockshop.de/
                    [cpc] => 0.26
                    [total_hits] => 1989
                )
            ...
     */
    public function getShopListFromDatabase($apiConfig, $minNumberOfOffers, $cpc)
    {
        /**
         * Variable initialization as an array
         */
        $shops = [];

        $dBConnection = new DBConnection();

        /**
         * Connect with '/hub.noctemque.com/data/db/shoplist_clone.db' db
         */
        $shopListDBConnection = $dBConnection->shopListDBConnection();

        $sql = "SELECT
                  *
                FROM
                    portal_entity AS a
                INNER JOIN shop_entity AS b ON a.id = b.portal_entity_id
                LEFT JOIN blacklisted_shop_id AS c ON b.shop_id = c.shop_id
                WHERE
                    a.portal_title = :portal
                    AND b.current_status = 1
                    AND b.total_hits >= :total_hits
                    AND b.cpc >= :cpc
                    AND c.shop_id IS NULL
                ORDER BY
                    b.shop_id";

        $results = $shopListDBConnection->prepare($sql);
        $results->bindParam(':portal', $apiConfig['user_key'], \PDO::PARAM_STR);
        $results->bindParam(':total_hits', $minNumberOfOffers, \PDO::PARAM_STR);
        $results->bindParam(':cpc', $cpc, \PDO::PARAM_STR);
        $results->bindParam(':blackListStr', $blackListStr, \PDO::PARAM_STR);
        $results->execute();

        /**
         * All data example. Here we select only shop_id
         *
         * @var $result = Array
            (
                [id] => 7
                [0] => 7
                [portal_title] => turbopreise_prod_API
                [1] => turbopreise_prod_API
                [portal_detail] =>
                [2] =>
                [created_at] => 2018-07-19 13:55:14
                [3] => 2018-07-16 09:20:02
                [updated_at] => 2018-07-19 13:55:14
                [4] => 2018-07-16 09:20:02
                [shop_entity_id] => 8668
                [5] => 8668
                [api_entity_id] => 1
                [6] => 1
                [portal_entity_id] => 7
                [7] => 7
                [shop_id] => 10025
                [8] => 10025
                [shop_name] => Ellerbrock-shop.de
                [9] => Ellerbrock-shop.de
                [company_url] => https://www.ellerbrockshop.de/
                [10] => https://www.ellerbrockshop.de/
                [total_hits] => 1997
                [11] => 1997
                [last_update_total_hit] => 2019-01-28 07:48:56
                [12] => 2019-01-28 07:48:56
                [current_status] => 1
                [13] => 1
                [cpc] => 0.26
                [14] => 0.26
                [reference_product_name] => Siemens KI24DA30 Einbau-KÃ¼hl-Gefrierautomat
                [15] => Siemens KI24DA30 Einbau-KÃ¼hl-Gefrierautomat
                [reference_product_offer_id] => 948104158
                [16] => 948104158
                [17] => 2018-07-19 13:55:14
                [18] => 2018-07-19 13:55:14
            )
         */
        foreach ($results as $shop) {
            $shops[$shop['shop_id']] = [
                'shop_id' => isset($shop['shop_id']) ? $shop['shop_id'] : '',
                'shop_name' => isset($shop['shop_name']) ? $shop['shop_name'] : '',
                'shop_url' => $this->helper->fixShopUrl(isset($shop['company_url']) ? $shop['company_url'] : ''),
                'cpc' => isset($shop['cpc']) ? $shop['cpc'] : '',
                'total_hits' => isset($shop['cpc']) ? $shop['total_hits'] : '',
            ];
        }

        return $shops;
    }

    /**
     * @param $id = '19739'
     * @return array|bool|\PDOStatement
     */
    public function getShopDetailByEntityId($id)
    {
        $dBConnection = new DBConnection();

        /**
         * Connect with '/hub.noctemque.com/data/db/shoplist_clone.db' db
         */
        $shopListDBConnection = $dBConnection->shopListDBConnection();

        $sql = "SELECT
                  a.portal_title,
                  b.*
                FROM
                    portal_entity AS a
                INNER JOIN shop_entity AS b ON a.id = b.portal_entity_id
                WHERE
                    b.shop_entity_id = :shop_entity_id";

        $results = $shopListDBConnection->prepare($sql);
        $results->bindParam(':shop_entity_id', $id, \PDO::PARAM_STR);
        $results->execute();

        return iterator_to_array($results);
    }

    /**
     * @param $domain = 'otto.de'
     * @param $apiListKeys = Array
        (
            [0] => turbopreise_prod_API
            [1] => turbopreise_s24
            [2] => connexity
        )
     * @return array|bool|\PDOStatement
     */
    public function getShopListByDomain($domain, array $apiListKeys)
    {
        /**
         * @var $apiListKeysStr = 'turbopreise_prod_API', 'turbopreise_s24', 'connexity'
         */
        $apiListKeysStr = '';

        foreach ($apiListKeys as $apiListKey) {
            $apiListKeysStr = $apiListKeysStr . "'$apiListKey', ";
        }

        $apiListKeysStr = trim($apiListKeysStr);
        $apiListKeysStr = rtrim($apiListKeysStr, ',');

        $dBConnection = new DBConnection();
        //$dBConnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        /**
         * Connect with '/hub.noctemque.com/data/db/shoplist_clone.db' db
         */
        $shopListDBConnection = $dBConnection->shopListDBConnection();

        $sql = "SELECT
                  a.portal_title,
                  b.*,
                  max (b.cpc) as cpc
                FROM
                    portal_entity AS a
                INNER JOIN shop_entity AS b ON a.id = b.portal_entity_id
                WHERE
                    b.current_status = '1'
                    AND b.domain_name_only = :domain_name_only
                    AND a.portal_title in ($apiListKeysStr)
                    GROUP BY a.portal_title
                    ORDER BY b.cpc DESC";

        $results = $shopListDBConnection->prepare($sql);
        $results->bindParam(':domain_name_only', $domain, \PDO::PARAM_STR);
        $results->execute();
        return iterator_to_array($results);
    }
}
