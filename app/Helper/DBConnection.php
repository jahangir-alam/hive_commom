<?php

namespace Hive\Helper;

class DBConnection
{
    /**
     * @var \Hive\Helper\Helper
     */
    protected $helper;

    /**
     * @var mixed = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();

        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = $this->helper->getConfig();
    }

    /**
     * Make sqlite3 connection to 'hub.noctemque.com/data/db/shoplist_clone.db' db file.
     *
     * @return \PDO
     */
    public function shopListDBConnection()
    {
        /**
         * @var $rootDir = '/var/www/html/blaupol'
         */
        $rootDir = $this->config['rootDir'];
        $dbDir = $rootDir . '/hub.noctemque.com/data/db/shoplist_clone.db';

        try {
            $dbh = new \PDO(sprintf('sqlite:%s', $dbDir));
            return $dbh;
        } catch (\PDOException $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: /hive.noctemque.com/traffic-includes/class/DBConnection.php",
                "Function: shopListDBConnection()",
                "``` {$e->getMessage()} ```"
            ]);
            echo 'Connection failed: ' . $e->getMessage();
            exit;
        }
    }

    /**
     * Make sqlite3 connection to 'hub.noctemque.com/data/best-of-shopping.net/db/click_bid_log.db' db file.
     *
     * @return \PDO
     */
    public function clickBidDBConnection()
    {
        /**
         * @var $rootDir = '/var/www/html/blaupol'
         */
        $rootDir = $this->config['rootDir'];
        $dbDir = $rootDir . '/hub.noctemque.com/data/best_of_shopping_net/db/click_bid_log.db';

        if (! file_exists($dbDir)) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: /hive.noctemque.com/traffic-includes/class/DBConnection.php",
                "Function: clickBidDBConnection()",
                "``` File Not Exist: $dbDir ```"
            ]);
        }

        try {
            $dbh = new \PDO(sprintf('sqlite:%s', $dbDir));
            return $dbh;
        } catch (\PDOException $e) {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: /hive.noctemque.com/traffic-includes/class/DBConnection.php",
                "Function: clickBidDBConnection()",
                "``` {$e->getMessage()} ```"
            ]);
            echo 'Connection failed: ' . $e->getMessage();
            exit;
        }
    }
}