<?php

namespace Hive\Helper\S24;

use Hive\Helper\Helper;
use Hive\Helper\ShopList;

class S24Helper
{
    /**
     * @var \Hive\Helper\Helper
     */
    protected $helper;

    /**
     * @var mixed = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();

        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = $this->helper->getConfig();
    }

    /**
     * @param $user = af734903
     * @param $pass = *****
     * @param $url = https://api.s24.com/v3/af734903/products?pageElements=1&shop=1576
     * @return mixed
     */
    public function getS24ApiContent($user, $pass, $url)
    {
        $curl = curl_init();

        //set aheader to accept json, otherwise the api will answer in xml
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "$user:$pass");

        //get data
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $data = curl_exec($curl);
        curl_close($curl);

        return json_decode($data, true);
    }

    /**
     * @param $apiConfig = Array
        (
            [api_name] => s24
            [api_base] => http://api.s24.com/v3
            [api_user] => 8c2a214e
            [api_pw] => *****
            [user_key] => turbopreise_s24
            [mc] => pseudo_mc_s24_turbopreise
            [shop_list_path] => hub.noctemque.com/data/shoplist/turbopreise_s24_shoplist.json
            [offers_cache] => turbopreise/storage/turbopreise/public/s24/offers
        )
     * @return array
     * @throws \Exception
     */
    public function makeS24Shops($apiConfig)
    {
        $data = [];

        /**
         * Get total shop list using API call not any .csv file
         *
         * @var string $url = 'https://api.s24.com/v3/8c2a214e/shops?pageElements=2000'
         */
        $url = "{$apiConfig['api_base']}/" . $apiConfig['api_user']. "/shops?pageElements=2000";

        /**
         * First element of this api call is = [totalResults] => 225
         *
         * {"totalResults":225,"page":1,"pageElements":2000,"shops":[{"id":"968","name":"123moebel.de","description":null,"links":[{"rel":"url","href":"http://www.123moebel.de"}],"imageResource":{"id":"8a118a8a3b8527b3014102b960a004cc","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/8a118a8a3b8527b3014102b960a004cc"}]},"serviceMail":"info@123moebel.de","serviceHotline":"0800-5 700 400","deliveryOption":"DHL, DPD","paymentMethod":"Vorkasse, Paypal, Kreditkarte, Rechnung, Lastschrift, Sofort\xc3\xbcberweisung, Ratenkauf"},{"id":"5064","name":"1a-Neuware","description":null,"links":[{"rel":"url","href":"https://www.1a-neuware.de/"}],"imageResource":{"id":"67a698ef86054d75ac3dc82defc756d3","base":"https://media03.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media03.s24.com/shop/67a698ef86054d75ac3dc82defc756d3"}]},"deliveryOption":"","paymentMethod":""},{"id":"1576","name":"21run.com","description":null,"links":[{"rel":"url","href":"http://www.21run.com/"}],"imageResource":{"id":"8a118a8a350b2b8d01351fa814290009","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/8a118a8a350b2b8d01351fa814290009"}]},"deliveryOption":"DHL","paymentMethod":"Vorkasse, Paypal, Kreditkarte, Nachnahme"},{"id":"5079","name":"4u2play","description":null,"links":[{"rel":"url","href":"https://www.4u2play.de/"}],"imageResource":{"id":"5881f96a43e14c73bf87e75a179ee5b7","base":"https://media01.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media01.s24.com/shop/5881f96a43e14c73bf87e75a179ee5b7"}]},"deliveryOption":"","paymentMethod":""},{"id":"2401","name":"ABOUT YOU","description":null,"links":[{"rel":"url","href":"http://www.aboutyou.de/"}],"imageResource":{"id":"8a118a8a456ea8cc014614a8b0120366","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/8a118a8a456ea8cc014614a8b0120366"}]},"serviceMail":"kundenservice@aboutyou.de","serviceHotline":"0800 / 30 15 085","deliveryOption":"Hermes, DHL","paymentMethod":"Paypal, Sofort\xc3\xbcberweisung, Rechnung, Kreditkarte, Ratenkauf","returnPolicy":"100 Tage R\xc3\xbcckgaberecht, kostenloser R\xc3\xbcckversand"},{"id":"2537","name":"agadon","description":null,"links":[{"rel":"url","href":"http://www.agadon.com/"}],"imageResource":{"id":"8a118a8a490d8a99014b5f0e62dd005d","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/8a118a8a490d8a99014b5f0e62dd005d"}]},"serviceMail":"office@agadon.com","serviceHotline":"0180 560 2022","deliveryOption":"DPD, UPS","paymentMethod":"Paypal, Kreditkarte, Rechnung, Vorkasse, Sofort\xc3\xbcberweisung","returnPolicy":"14 Tage R\xc3\xbcckgaberecht"},{"id":"5264","name":"Akkushop","description":null,"links":[{"rel":"url","href":"https://www.akkushop.de/"}],"imageResource":{"id":"b8facbeec87b4ce4a0e52814c460f989","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/b8facbeec87b4ce4a0e52814c460f989"}]},"deliveryOption":"","paymentMethod":""},{"id":"5036","name":"Aktivwelt","description":null,"links":[{"rel":"url","href":"https://www.aktivwelt.de/"}],"imageResource":{"id":"1d0b87b426cd45eaa12b414671e0f9b0","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/1d0b87b426cd45eaa12b414671e0f9b0"}]},"deliveryOption":"","paymentMethod":""},{"id":"5123","name":"ALBERTKREUZ","description":null,"links":[{"rel":"url","href":"https://www.albert-kreuz.de/"}],"imageResource":{"id":"b9790e44a09e4f94b61825de40de6009","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/b9790e44a09e4f94b61825de40de6009"}]},"deliveryOption":"","paymentMethod":""},{"id":"5068","name":"allzweck","description":null,"links":[{"rel":"url","href":"https://www.allzweck.de/"}],"imageResource":{"id":"1ef749440fbd4e1ba734d3f032b00fb5","base":"https://media03.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media03.s24.com/shop/1ef749440fbd4e1ba734d3f032b00fb5"}]},"deliveryOption":"","paymentMethod":""},{"id":"2350","name":"AMBIENDO","description":null,"links":[{"rel":"url","href":"http://www.ambiendo.de"}],"imageResource":{"id":"3dd3a39931154d5da98e3dabf97cbf46","base":"https://media04.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media04.s24.com/shop/3dd3a39931154d5da98e3dabf97cbf46"}]},"serviceMail":"service@ambiendo.de","serviceHotline":"0251 590 66 960","deliveryOption":"DHL","paymentMethod":"Vorkasse, Paypal, Kreditkarte, Rechnung, Sofort\xc3\xbcberweisung, Lastschrift","returnPolicy":"33 Tage R\xc3\xbcckgaberecht"},{"id":"5243","name":"Amilian Baby Shop","description":null,"links":[{"rel":"url","href":"https://www.amilian.de/"}],"imageResource":{"id":"ac2b77740f5347bba222a88807468326","base":"https://media03.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media03.s24.com/shop/ac2b77740f5347bba222a88807468326"}]},"deliveryOption":"","paymentMethod":""},{"id":"2290","name":"Atelier Goldner Schnitt","description":null,"links":[{"rel":"url","href":"http://www.ateliergs.de"}],"imageResource":{"id":"bb47f739e3e646b1a39ebf2b7e87ab7e","base":"https://media01.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media01.s24.com/shop/bb47f739e3e646b1a39ebf2b7e87ab7e"}]},"serviceMail":"kundenservice@ateliergs.de","serviceHotline":"09251 / 4646","deliveryOption":"DHL","paymentMethod":"Rechnung"},{"id":"2762","name":"auna","description":null,"links":[{"rel":"url","href":"http://auna.de/"}],"imageResource":{"id":"b98fd72f00cc4bd58437991c8fba1628","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/b98fd72f00cc4bd58437991c8fba1628"}]},"serviceMail":"info@auna.de","serviceHotline":"030 - 408 173 509","deliveryOption":"","paymentMethod":"Vorkasse, Paypal, Kreditkarte, Nachnahme"},{"id":"2603","name":"Avena","description":null,"links":[{"rel":"url","href":"https://www.avena.de"}],"imageResource":{"id":"8a118a8a490d8a99014ce0f81cf70086","base":"https://media04.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media04.s24.com/shop/8a118a8a490d8a99014ce0f81cf70086"}]},"serviceHotline":"(0180) 512 0 512","deliveryOption":"","paymentMethod":"Paypal, Kreditkarte, Rechnung, Lastschrift"},{"id":"2408","name":"Babista","description":null,"links":[{"rel":"url","href":"http://www.babista.de/"}],"imageResource":{"id":"8a118a8a490d8a99014c2327ece1006f","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/8a118a8a490d8a99014c2327ece1006f"}]},"deliveryOption":"","paymentMethod":""},{"id":"5041","name":"BACKPACKER-STORE.DE","description":null,"links":[{"rel":"url","href":"https://www.backpacker-stores.de"}],"imageResource":{"id":"a5a280230738481c922d96cbdb1d9935","base":"https://media03.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media03.s24.com/shop/a5a280230738481c922d96cbdb1d9935"}]},"deliveryOption":"","paymentMethod":""},{"id":"48","name":"Baur Versand","description":null,"links":[{"rel":"url","href":"http://www.baur.de"}],"imageResource":{"id":"8a118a8a490d8a99014b73ca718e005f","base":"https://media03.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media03.s24.com/shop/8a118a8a490d8a99014b73ca718e005f"}]},"deliveryOption":"Hermes","paymentMethod":"Paypal, Kreditkarte, Rechnung, Lastschrift, Sofort\xc3\xbcberweisung, Ratenkauf"},{"id":"4989","name":"Beckhuis.com","description":null,"links":[{"rel":"url","href":"https://www.beckhuis.com"}],"imageResource":{"id":"fda6362f34c0403b89a7f8c31b9ace06","base":"https://media02.s24.com/shop/","type":"logo","links":[{"rel":"self","href":"https://media02.s24.com/shop/fda6362f34c0403b89a7f8c31b9ace06"}]},"deliveryOption":"","paymentMethod":""},{"id":"1417","name":"BELIANI","description":null,"links":[{"rel":"url","href":"http://www.beliani.de"}],"
         *
         * @var $product = See 'data/examples/log/s24-single-product.log'
         *
         * Array
            (
                [totalResults] => 225
                [page] => 1
                [pageElements] => 2000
                [shops] => Array
                    (
                    [0] => Array
                        (
                            [id] => 968
                            [name] => 123moebel.de
                            [description] =>
                            [links] => Array
                            (
                            [0] => Array
                            (
                            [rel] => url
                            [href] => http://www.123moebel.de
                            )
                            )
                            ...
                        )
                    )
                ...
            )
         */
        $shops = $this->getS24ApiContent($apiConfig['api_user'], $apiConfig['api_pw'], $url);
        if (isset($shops['shops']) && is_array($shops['shops']) && count($shops['shops'])) {
            foreach ($shops['shops'] as $shop) {
                $shopId = isset($shop['id']) && $shop['id'] ? $shop['id'] : null;

                /**
                 * @var $cpcHitData = Array
                    (
                        [total_hits] => 45
                        [cpc] => 0,26
                    )
                 */
                $cpcHitData = $this->getS24TotalHitCpc($apiConfig, $shopId);

                $data[$shopId] = [
                    'shop_id' => $shopId,
                    'shop_name' => isset($shop['name']) && $shop['name'] ? $shop['name'] : null,
                    'shop_url' => $this->helper->fixShopUrl(
                        isset($shop[$shopId]['links'][0]['href'])
                            ? $shop[$shopId]['links'][0]['href']
                            : ''
                    ),
                    'cpc' => number_format(str_replace(',', '.', $cpcHitData['cpc']), 2),
                    'total_hits' => $cpcHitData['total_hits'],
                ];
            }
        }

        return $data;
    }

    /**
     * URL: http://hub.noctemque.com/get-offer/add-total-hit-by-portal?portal=s24-csv-noctemque
     *
     * It takes 73 seconds for all shops (now total shop = 280)
     * @todo = when shops increase it will not cover within 2 minutes, we need to rethink about it.
     *
     * @param $apiConfig = Array
        (
            [api_name] => s24
            [api_base] => http://api.s24.com/v3
            [api_user] => 8c2a214e
            [api_pw] => *****
            [user_key] => turbopreise_s24
            [mc] => pseudo_mc_s24_turbopreise
            [shop_list_path] => hub.noctemque.com/data/shoplist/turbopreise_s24_shoplist.json
            [offers_cache] => turbopreise/storage/turbopreise/public/s24/offers
        )
     * @param $shopId = 2944
     * @return array = Array
        (
            [total_hits] => 45
            [$cpc] => 0,26
        )
     */
    public function getS24TotalHitCpc($apiConfig, $shopId)
    {
        /**
         * @var string $apiUser = '8c2a214e'
         */
        $apiUser = $apiConfig['api_user'];

        /**
         * @var string $apiPass = '*****'
         */
        $apiPass = $apiConfig['api_pw'];

        /**
         * @var $url = 'https://api.s24.com/v3/af734903/products?pageElements=1&shop=2944'
         */
        $url = "{$apiConfig['api_base']}/$apiUser/products?pageElements=1&shop=$shopId";

        /**
         * First element of this api call is = [totalResults] => 197
         *
         * @var $product = See 'data/examples/log/s24-single-product.log'
         */
        $product = $this->getS24ApiContent($apiUser, $apiPass, $url);

        /**
         * @var integer $totalHits = 197
         */
        $totalHits = isset($product['totalResults']) ? $product['totalResults'] : '';

        $cpc = isset($product['products'][0]['shops']['shopitems'][0]['costs']['earnings']['price'])
            ? $product['products'][0]['shops']['shopitems'][0]['costs']['earnings']['price'] : 0;

        return [
            'total_hits' => $totalHits,
            'cpc' => $cpc
        ];
    }

    /**
     * URL: http://hub.noctemque.com/get-offer/add-total-hit-by-portal?portal=s24-csv-noctemque
     *
     * It takes 73 seconds for all shops (now total shop = 280)
     * @todo = when shops increase it will not cover within 2 minutes, we need to rethink about it.
     *
     * @param $apiConfig = Array
        (
            [api_name] => s24
            [api_base] => http://api.s24.com/v3
            [api_user] => 8c2a214e
            [api_pw] => *****
            [user_key] => turbopreise_s24
            [mc] => pseudo_mc_s24_turbopreise
            [shop_list_path] => hub.noctemque.com/data/shoplist/turbopreise_s24_shoplist.json
            [offers_cache] => turbopreise/storage/turbopreise/public/s24/offers
        )
     * @param $shopId = 2944
     * @param $qty = 50
     * @return array = Array
        (
            [total_hits] => 45
            [$cpc] => 0,26
        )
     */
    public function getS24Offers($apiConfig, $shopId, $qty)
    {
        /**
         * @var string $apiUser = '8c2a214e'
         */
        $apiUser = $apiConfig['api_user'];

        /**
         * @var string $apiPass = '*****'
         */
        $apiPass = $apiConfig['api_pw'];

        /**
         * @var $url = 'https://api.s24.com/v3/af734903/products?pageElements=1&shop=2944'
         */
        $url = "{$apiConfig['api_base']}/$apiUser/products?pageElements=$qty&shop=$shopId";

        /**
         * First element of this api call is = [totalResults] => 197
         *
         * @var $product = See 'data/examples/log/s24-single-product.log'
         */
        $product = $this->getS24ApiContent($apiUser, $apiPass, $url);

        return $product;
    }
}
