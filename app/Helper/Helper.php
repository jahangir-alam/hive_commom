<?php

namespace Hive\Helper;

class Helper
{
    /**
     * @var mixed  = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = include dirname(dirname(__File__)) . "/config/Config.class.php";
    }

    /**
     * Get configuration from 'traffic-includes/config/Config.class.php' file.
     *
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Notify the developer through slack.
     *
     * Which channel please ??
     *
     * Demo Format:
     *
        notifyDeveloperUsingSlack([
            "Source: module/Merchant/src/Controller/Creates24AccToGmcController.php",
            "Function: indexAction()",
            "``` Test Code... ```"
        ]);
     *
     * @param array $message = [
                "Source: module/Merchant/src/Controller/Creates24AccToGmcController.php",
                "Function: indexAction()",
                "``` Test Code... ```"
            ]
     */
    public function notifyDeveloperUsingSlack($message = [])
    {
        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] . "\n" : "";
        $data = [
            'text'  =>  $host. implode(PHP_EOL, $message),
        ];

        $data_string = json_encode($data);

        $ch = curl_init('https://hooks.slack.com/services/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');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ]
        );

        curl_exec($ch);
        curl_close($ch);
    }

    /**
     * @param object $dbConnection = \PDO
     * @param array $data
     * @return bool
     */
    public function flatInsert($dbConnection, $tableName, $data)
    {
        try {
            $fieldsArr = array_keys($data);
            $columns = implode(', ', $fieldsArr);
            $columnsValuse = ':' . str_replace(', ', ', :', $columns);

            $insertQuery = "INSERT INTO $tableName ($columns) VALUES ($columnsValuse)";
            $model = $dbConnection->prepare($insertQuery);
            $model->execute($data);

            return $dbConnection->lastInsertId();
        } catch (\PDOException $e) {
            $this->notifyDeveloperUsingSlack([
                "Source: traffic-includes/class/Helper.php",
                "Function: flatInsert()",
                "``` {$e->getMessage()} ```"
            ]);
            return false;
        }
    }

    public function fixShopUrl($shopUrl)
    {
        /**
         * @var $shopUrlArr = 'https://www.otto.de'
         */
        $shopUrlArr = parse_url($shopUrl, PHP_URL_HOST);

        if (empty($shopUrlArr)) {
            $shopUrlArr = $shopUrl;
        }

        /**
         * 2019-02-04
         * Source: skype
         * we need to remove http:// or https:// + www
         */
        return strtolower(str_replace('www.', '', $shopUrlArr));
//        try {
//            $extract = new \LayerShifter\TLDExtract\Extract();
//
//            # For domain 'shop.github.com'
//            $result = $extract->parse($shopUrl);
//            return $result->getRegistrableDomain();
//        } catch (\Exception $exception) {
//            $this->notifyDeveloperUsingSlack(
//                "traffic-includes/class/Helper.php",
//                "fixShopUrl()",
//                "``` {$exception->getMessage()} ```"
//            );
//            exit;
//        }
    }

    /**
     * kelkoo signs url first before getting offer data.
     * We have to sign url with this function before every kelkoo api call.
     *
     * @param $urlDomain
     * @param $urlPath
     * @param $partner
     * @param $key
     * @return string
     */
    public function kelkooUrlSigner($urlDomain, $urlPath, $partner, $key)
    {
        settype($urlDomain, 'String');
        settype($urlPath, 'String');
        settype($partner, 'String');
        settype($key, 'String');

        $URL_sig = "hash";
        $URL_ts = "timestamp";
        $URL_partner = "aid";
        $URLreturn = "";
        $URLtmp = "";
        $s = "";
        // get the timestamp
        $time = time();

        // replace " " by "+"
        $urlPath = str_replace(" ", "+", $urlPath);
        // format URL
        $URLtmp = $urlPath . "&" . $URL_partner . "=" . $partner . "&" . $URL_ts . "=" . $time;

        // URL needed to create the tokken
        $s = $urlPath . "&" . $URL_partner . "=" . $partner . "&" . $URL_ts . "=" . $time . $key;
        $tokken = "";
        $tokken = base64_encode(pack('H*', md5($s)));
        $tokken = str_replace(array("+", "/", "="), array(".", "_", "-"), $tokken);
        $URLreturn = $urlDomain . $URLtmp . "&" . $URL_sig . "=" . $tokken;
        return $URLreturn;
    }

    /**
     * Get kelkoo api data only through this function
     *
     * @param $url
     * @return bool|false|mixed|\SimpleXMLElement|string
     */
    public function getKelkooUrl($url)
    {
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=> "Accept-Encoding: gzip,deflate"
            )
        );
        $context = stream_context_create($opts);
        $content = file_get_contents($url, false, $context);
        $content = gzinflate(substr($content,10,-8));

        /**
         * Custom added
         * Not exist in documentation
         */
        $content = simplexml_load_string($content);
        $content = json_encode($content);
        $content = json_decode($content, true);
        return $content;
    }
}
