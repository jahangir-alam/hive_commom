<?php

namespace Hive\Helper\Billiger;

use Hive\Helper\Helper;
use Hive\Helper\ShopList;

class BilligerHelper
{
    /**
     * Request size at a time
     */
    const BATCH_SIZE = 100;

    /**
     * @var \Hive\Helper\Helper
     */
    protected $helper;

    /**
     * @var mixed = 'traffic-includes/config/Config.class.php'
     */
    protected $config;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();

        /**
         * Location: 'traffic-includes/config/Config.class.php'
         */
        $this->config = $this->helper->getConfig();
    }

    /**
     * Basic Curl call function
     *
     * @param string $url = ??
     * @return mixed = ??
     */
    public function fileGetContentsCurl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);

        $retries = 0;

        $data = curl_exec($ch);

        while (((curl_errno($ch) > 0)) && ($retries < 3)) {
            $data = curl_exec($ch);
            $retries++;
        }

        curl_close($ch);

        return $data;
    }

    /**
     * Make Api call with http curl request.
     *
     * @param $function string [example: 'get_shops']
     * @param $params array [empty]. Its empty intentionally. So we will able to receive all shop list for all country and inactive too
     * After that, we can filter as required.
     * @param $apiConfig array [example: Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => ca_nosy
            [api_pw] => *****
        )]
     * @return mixed
     */
    public function apiCall($function, $params, $apiConfig)
    {
        $url = $apiConfig['api_base'];
        $user = $apiConfig['api_user'];
        $pass = $apiConfig['api_pw'];

        $host = "http://$user:$pass@$url";
        $path = "$function?";

        foreach ($params as $key => $value) {
            $path .= $key . '=' . $value . '&';
        }
        if ($path[strlen($path) - 1] == "&") {
            $path = substr($path, 0, strlen($path) - 1);
        }
        $uri = $host . '/' . $path;

        $json = $this->fileGetContentsCurl($uri);

        return $json;
    }

    /**
     * @param $apiConfig = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => turbopreise_prod_API
            [api_pw] => *****
            [mc] => Kjqy8tnekP63
            [shop_list_path] => /var/www/html/blaupol/hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json
            [offers_cache] => /var/www/html/blaupol/hive.noctemque.com/traffic-includes/turbopreise/html/offers_cache
        )
     * @param $blackListedShops = Array
        (
            [1019] => 1019
            [1021] => 1021
        )
     * @param $minNumberOfOffers = 10
     * @param $cpc = 0.10
     * @return array|mixed
     */
    public function makeBilligerShops($apiConfig, $minNumberOfOffers, $cpc, $blackListedShops)
    {

        /**
         * @var array $shops for return statement
         */
        $shops = [];

        /**
         * Its empty intentionally. So we will able to receive all shop list for all country and inactive too
         * After that, we can filter as required.
         */
        $params = [];

        $json = $this->apiCall("get_shops", $params, $apiConfig);

        $shopsAll = json_decode($json, true);

        /**
         * Its its a large array. We will split it into chunk of 100 element. So we will get all information through API call per chunk
         *
         * @var $itemChunk = Array
            (
                [0] => Array
                (
                    [0] => Array
                        (
                            [userreview_count] => 0
                            [company_url] => https://www.123-reifen.ch/
                            [shop_id] => 21268
                            [company_street] => Brühlstr. 11
                            ...
                    ...
                ...
         */
        $itemChunk = array_chunk($shopsAll, self::BATCH_SIZE, true);

        foreach ($itemChunk as $items) {
            /**
             * Api call takes 3-6 Seconds with 1 products.
             *
             * @var $response = Array
            (
            [16091] => Array
            (
            [shop_id] => 16091
            [shop_name] => all-4-baby.de
            [shop_url] => https://www.all-4-baby.de/
            [cpc] => 0.215
            [total_hits] => 1607
            )
            ...
             */
            $response = $this->callSingleCurlWithMultiRequest($apiConfig, $items, $minNumberOfOffers, $cpc, $blackListedShops);

            if (isset($response) && is_array($response) && count($response)) {
                $shops = ($shops + $response);
            }
        }

        return $shops;
    }

    /**
     * Single api call with multiple URL request
     *
     * According to http://api.billiger.de/content/2.0/docs/index.html#multi-request We can pass multiple API URL
     * using single request
     *
     * Execution URL: http://hub.noctemque.com/get-offer/add-total-hit-discountheld
     *
     * @param array $api = Array
    (
    [api_name] => api.billiger.de
    [api_base] => api.billiger.de/content/2.0
    [api_user] => discountheld_prod_API
    [api_pw] => *****
    [status] => 1
    )
     * @param object $items = Array [count($item) = 200]
    (
    [0] => Array
    (
    [shop_id] => 18355
    )
    [1] => Array
    (
    [shop_id] => 20437
    )
    )
     * @param $blackListedShops = Array
    (
    [1019] => 1019
    [1021] => 1021
    )
     * @param $minNumberOfOffers = 10
     * @param $cpc = 0.10
     * @return mixed = Array
    (
    [16091] => Array
    (
    [shop_id] => 16091
    [shop_name] => all-4-baby.de
    [shop_url] => https://www.all-4-baby.de/
    [cpc] => 0.215
    [total_hits] => 1607
    )
    ...
     */
    public function callSingleCurlWithMultiRequest($api, $items, $minNumberOfOffers, $cpc, $blackListedShops)
    {
        /**
         * Preserve shop info to use later after making 'multi_request'
         */
        $primaryShopInfo = [];

        /**
         * Initialize the array for next processing
         */
        $data = [];

        /**
         * We need to link (URL) together in a chain or series like a single string
         *
         * @var string $urls
         */
        $urls = '';

        /**
         * @var array $value = Array
        (
        [userreview_count] => 0
        [company_url] => https://www.123pneus.fr/
        [shop_id] => 21004
        [company_street] => Brühlstr. 11
        [company_city] => Hannover
        [company_country] => Deutschland
        [company_name] => Delticom AG
        [certificates] => Array
        (
        )
        [logo_url] => https://cdn.billiger.com/shops/x/21004.gif
        [company_zip_code] => 30169
        [support_phone] =>
        [description] =>
        [has_billiger_cert] =>
        [country_of_activity] => FR
        [is_online] => 1
        [support_mail] => billiger@delti.com
        [userreview_rating] => 0
        [mobile_optimized] =>
        [name] => 123 Pneus.fr
        ...
        )
         */
        foreach ($items as $value) {
            $shopId = $value['shop_id'];
            $shopName = $value['name'];
            $shopUrl = $value['company_url'];

            $primaryShopInfo[$shopId] = [
                'shop_name' => $shopName,
                'shop_url' => $shopUrl
            ];

            /**
             * @var array $params = Array
            (
            [filter:shop] => 21636
            [doctype] => offer,product_offer
            [filter_flags] => off
            [fuzzy] => 0
            [page_size] => 100
            [page] => 0
            [sort] => clickout_relevance
            )
             */
            $params = [
                'filter:shop' => $shopId,
                'doctype' => 'offer,product_offer',
                'filter_flags' => 'off',
                'fuzzy' => 0,
                'page_size' => 1,
                'page' => 0,
                'sort' => 'clickout_relevance', // its standard anyway, but just to be sure
            ];

            /**
             * @var $urls = http://toppreistipps_prod_API:FISmmlz4pB@api.billiger.de/content/2.0/search?filter:shop=20921&doctype=offer,product_offer&filter_flags=off&fuzzy=0&page_size=1&page=0
             */
            $urls .= $this->makeApiUrl($api, 'search', $params) . PHP_EOL;
        }

        /**
         * We need to continue with http not https. Into discountheld, its http
         *
         * @var string $uri = 'http://discountheld_prod_API:*****@api.billiger.de/content/2.0/multi_request'
         */
        $uri = 'http://'.$api['api_user'].':'.$api['api_pw'].'@'.$api['api_base'].'/multi_request';

        /**
         * @var array $responses = Array
        (
        [http://discountheld_prod_API:1t976x2Dqy@api.billiger.de/content/2.0/search?filter:shop=12917&doctype=offer,product_offer&filter_flags=off&sort=clickout_relevance&page_size=1&page=0] => Array
        (
        [total_hits] => 2110
        [didyoumean] => Array
        (
        )
        [hits] => Array
        (
        [0] => Array
        (
        [shop] => agadon - wärme und mehr
        [price_per_unit] =>
        [availability_code] => red
        [brand_id] => 1743022
        [clickout_link] => https://www.billiger.de/common/modules/api/cmodul?p=7MrN6mJ5Breqcup7-RX7zhSn6WuKiUTMudNbazqXXDB501trOpdcMFoL94OGvmOHiU3WLPv7BEDVunknuQ7eY0MbTinpayzj9pL4nG3_Ksn-kLO5NWLWFEeF8ZBoGX1q3AALB1HyiDX&id=676955808&mc=b6xSNzH2f8ly
        [pzn] => Array
        ...
        )
         * Api call takes 3-6 Seconds with 1 products.
         */
        $jsonResponse = $this->getContentsCurlWithPostBody($uri, $urls);
        $responses = json_decode($jsonResponse, true);

        if (isset($_GET['debug'])) {
            echo "<br />Got total API response count = : ". count($responses) . " Responses<br />";
        }

        if (isset($responses) && is_array($responses) && count($responses)) {
            /**
             * The entire URL return as a key
             *
             * @var string $key = 'http://discountheld_prod_API:1t976x2Dqy@api.billiger.de/content/2.0/search?filter:shop=9782&doctype=offer,product_offer&filter_flags=off&sort=clickout_relevance&page_size=1&page=0'
             */

            foreach ($responses as $key => $response) {
                /**
                 * @var int $totalHits = 15
                 */
                $totalHits = isset($response['total_hits']) && $response['total_hits'] ? $response['total_hits'] : 0;

                /**
                 * @var float $responseCpc = 0.26
                 */
                $responseCpc = isset($response['hits'][0]['cpc']) && $response['hits'][0]['cpc'] ? $response['hits'][0]['cpc'] : 0;

                /**
                 * @var int $shop_id = 16635
                 */
                $shop_id = $this->getShopIdFromUrl($key);

                /**
                 * Filter blacklisted shops.
                 * Check min offer.
                 * Check cpc.
                 */
                if (in_array($shop_id, $blackListedShops)
                    || $totalHits < $minNumberOfOffers
                    || $responseCpc < $cpc) {
                    continue;
                }

                $data[$shop_id] = [
                    'shop_id' => $shop_id,
                    'shop_name' => isset($primaryShopInfo[$shop_id]['shop_name']) ? $primaryShopInfo[$shop_id]['shop_name'] : '',
                    'shop_url' => $this->helper->fixShopUrl(isset($primaryShopInfo[$shop_id]['shop_url']) ? $primaryShopInfo[$shop_id]['shop_url'] : ''),
                    'cpc' => $responseCpc,
                    'total_hits' => $totalHits,
                ];
            }
        } else {
            $this->helper->notifyDeveloperUsingSlack([
                "Source: /hive.noctemque.com/traffic-includes/class/Api.php",
                "Function: callSingleCurlWithMultiRequest()",
                "``` No API response! ```",
                "``` ".print_r($api, 1)." ```"
            ]);

            return false;
        }

        return $data;
    }

    /**
     * Creates suitable url for making api call.
     *
     * @param array $api = Array
        (
            [api_base] => api.billiger.de/content/2.0
            [api_user] => discountheld_prod_API
            [api_pw] => *****
        )
     * @param string $function = 'search'
     * @param array $params = Array (
     *  $params['filter:shop'] = $shopId;
        $params['doctype'] = 'offer,product_offer';
        $params['filter_flags'] = 'off';
        $params['sort'] = 'clickout_relevance';
        $params['page_size'] = 1; //get only one offer intentionally, otherwise it will bring all available offer
        $params['page'] = 0;
     * )
     *
     * @return string = http://discountheld_prod_API:1t976x2Dqy@api.billiger.de/content/2.0/search?filter:shop=11658&doctype=offer,product_offer&filter_flags=off&sort=clickout_relevance&page_size=1&page=0
     */
    public function makeApiUrl($api, $function, $params)
    {
        $url = $api['api_base'];
        $user = $api['api_user'];
        $passwd = $api['api_pw'];
        $host = "http://$user:$passwd@$url";
        $path = "$function?";

        foreach ($params as $key => $value) {
            $path .= $key . '=' . $value . '&';
        }

        if ($path[strlen($path) - 1] == "&") {
            $path = substr($path, 0, strlen($path) - 1);
        }

        $uri = $host . '/' . $path;

        return $uri;
    }

    /**
     * Multiple response comes with the url as response array key.
     * When we get response from multiple request response,
     * We have to find the shop id from response url to identify the actual response for.
     *
     * @param  string $url = 'http://discountheld_prod_API:1t976x2Dqy@api.billiger.de/content/2.0/search?filter:shop=9782&doctype=offer,product_offer&filter_flags=off&sort=clickout_relevance&page_size=1&page=0'
     * @return mixed = '9782' | null
     */
    public function getShopIdFromUrl($url)
    {
        /**
         * ??
         */
        $parts = parse_url($url);

        /**
         * ??
         */
        $queryArr = explode(':', $parts['query']);

        /**
         * ??
         */
        $mainQuery = isset($queryArr[1]) ? $queryArr[1] : '';
        parse_str($mainQuery, $query);

        return isset($query['shop']) ? $query['shop'] : null;
    }

    /**
     * Call Curl Request with post body
     *
     * @param $url = 'http://toppreistipps_prod_API:FISmmlz4pB@api.billiger.de/content/2.0/multi_request'
     * @param $body = 'http://toppreistipps_prod_API:FISmmlz4pB@api.billiger.de/content/2.0/search?filter:shop=20921&doctype=offer,product_offer&filter_flags=off&fuzzy=0&page_size=1&page=0
    http://toppreistipps_prod_API:FISmmlz4pB@api.billiger.de/content/2.0/search?filter:shop=14482&doctype=offer,product_offer&filter_flags=off&fuzzy=0&page_size=1&page=0
    http://toppreistipps_prod_API:FISmmlz4pB@api.billiger.de/content/2.0/search?filter:shop=14410&doctype=offer,product_offer&filter_flags=off&fuzzy=0&page_size=1&page=0'
     * @return mixed
     */
    public function getContentsCurlWithPostBody($url, $body)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

        $retries = 0;

        $data = curl_exec($ch);

        while (((curl_errno($ch) > 0)) && ($retries < 3)) {
            $data = curl_exec($ch);
            $retries++;
        }

        curl_close($ch);

        return $data;
    }
}
