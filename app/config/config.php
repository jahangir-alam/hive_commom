<?php
/**
 * Created by Md. Jahangir Alam <jahangir@sulaco-tec.com>.
 * User: sulaco-tec
 * Date: 10/23/19
 * Time: 9:31 PM
 */

error_reporting(0);

/**
 * @constant traffic_includes_HOME = `/usr/home/ejdqpb/public_html/hive.noctemque.com/traffic-includes/`
 */
define ('HIVE_ROOT', dirname(dirname(dirname(__File__))) . DIRECTORY_SEPARATOR);


/**
 * Where you use this variable and why?
 */
$domain_whitelist[] = 'discountheld';

/**
 * Where you use this variable ?
 */
//openssl-encryption
$salt = "peraspera";
$iv = "parturiunt monte";
$skey = "15833b7581e592f7";

/**
 * Can we use 'dirname(dirname(__File__))' ?
 * @var string $all_root = '/usr/home/ejdqpb/public_html/hive.noctemque.com/traffic-includes/../../'
 */
$all_root = HIVE_ROOT .'../../';

/**
 * Its a default value
 * @var int $min_offers_per_shop = 10
 */
$min_offers_per_shop=10;

//------------ s24.com ------------------------------------------------------------
$api['discountheld_s24']['api_base'] = "http://api.s24.com/v3";
$api['discountheld_s24']['api_user'] = "af734903";
$api['discountheld_s24']['api_pw'] = "fexb0jou2vl0aukchivinehaa3fayrpl";
$api['discountheld_s24']['user_key'] = "discountheld_s24";
$api['discountheld_s24']['mc'] = "pseudo_mc_s24_discountheld";
$api['discountheld_s24']['name'] = "s24";
$api['discountheld_s24']['root'] = $all_root;
$api['discountheld_s24']['web_root'] = $api['discountheld_s24']['root'] . 'discountheld/';
$api['discountheld_s24']['click_db_path'] = $api['discountheld_s24']['web_root'].'html/click/db/';
$api['discountheld_s24']['shop_list_path'] = $all_root .'hub.noctemque.com/data/shoplist/discountheld_s24_shoplist.json';
$api['discountheld_s24']['base_url'] = 'https://discountheld.de';

$api['discountheld_s24']['leverage_ratio_st'] = 0.7; //was 0.33
//$api['discountheld_s24']['leverage_ratio_st'] = 10;
$api['discountheld_s24']['leverage_ratio_st_pharmacies'] = 10;

$api['discountheld_s24']['leverage_ratio_no_st'] = 0.7; // a-clicks divided by b-clicks
$api['discountheld_s24']['max_leverage_no_st'] = 1; //do not exceed this ratio
//$api['discountheld_s24']['leverage_ratio_no_st'] = 10; // a-clicks divided by b-clicks
//$api['discountheld_s24']['max_leverage_no_st'] = 10; //do not exceed this ratio

$api['discountheld_s24']['max_clicks_per_day_for_stuffing_shops'] = 10; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['discountheld_s24']['min_days_to_decide_stuffing_or_leveraging'] = 5; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed

//------------ discountheld ------------------------------------------------------------
$api['discountheld']['api_base']="api.billiger.de/content/2.0";
$api['discountheld']['api_user']="discountheld_prod_API";
$api['discountheld']['api_pw']="1t976x2Dqy";
$api['discountheld']['user_key'] = "discountheld_prod_API";
$api['discountheld']['mc']="b6xSNzH2f8ly";
$api['discountheld']['name']="discountheld";
$api['discountheld']['root']=$all_root.'discountheld/';
$api['discountheld']['web_root']=$api['discountheld']['root'].'html/';
$api['discountheld']['click_db_path']=$api['discountheld']['web_root'].'click/db/';
$api['discountheld']['shop_list_path'] = $all_root.'hub.noctemque.com/data/shoplist/discountheld_prod_API_shoplist.json';
$api['discountheld']['base_url']='http://discountheld.de';

$api['discountheld']['leverage_ratio_st']=1.3;
$api['discountheld']['leverage_ratio_st_pharmacies']=20;

$api['discountheld']['leverage_ratio_no_st']=1.5; // a-clicks divided by b-clicks
$api['discountheld']['max_leverage_no_st']=1.5; //do not exceed this ratio

$api['discountheld']['max_clicks_per_day_for_stuffing_shops']=10; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['discountheld']['min_days_to_decide_stuffing_or_leveraging']=5; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed

//------------ preispudel ------------------------------------------------------------
$api['preispudel']['api_base']="api.billiger.de/content/2.0";
$api['preispudel']['api_user']="Preispudel_prod_API";
$api['preispudel']['api_pw']="wTGX1i8X3X";
$api['preispudel']['user_key'] = "Preispudel_prod_API";
$api['preispudel']['mc']="LlZ91pz8Oy9E";
$api['preispudel']['name']="preispudel";
$api['preispudel']['root']=$all_root.'discountheld/';	//	Exception!
$api['preispudel']['web_root']=$api['discountheld']['root'].'html/';	//	Exception!
$api['preispudel']['click_db_path']=$api['discountheld']['web_root'].'click/db/';
$api['preispudel']['shop_list_path'] = $all_root . 'hub.noctemque.com/data/shoplist/Preispudel_prod_API_shoplist.json';
$api['preispudel']['base_url']='http://discountheld.de';	//	Exception!

$api['preispudel']['max_clicks_per_day_for_stuffing_shops']=10; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['preispudel']['min_days_to_decide_stuffing_or_leveraging']=5; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed


//------------ turbopreise ------------------------------------------------------------
$api['turbopreise']['api_base']="api.billiger.de/content/2.0";
$api['turbopreise']['api_user']="turbopreise_prod_API";
$api['turbopreise']['api_pw']="SVL8EkcGnc";
$api['turbopreise']['user_key'] = "turbopreise_prod_API";
$api['turbopreise']['mc']="Kjqy8tnekP63";
$api['turbopreise']['name']="turbopreise";
$api['turbopreise']['root']=$all_root.'turbopreise/';
$api['turbopreise']['web_root']=$api['turbopreise']['root'].'html/';
$api['turbopreise']['click_db_path']=$api['turbopreise']['web_root'].'click/db/';
$api['turbopreise']['shop_list_path'] = $all_root . 'hub.noctemque.com/data/shoplist/turbopreise_prod_API_shoplist.json';
$api['turbopreise']['base_url']='http://turbopreise.de';

$api['turbopreise']['leverage_ratio_st']=1.1; //was: 1.1
$api['turbopreise']['leverage_ratio_st_pharmacies']=20; //was 5

$api['turbopreise']['leverage_ratio_no_st']=0.27; // a-clicks divided by b-clicks, was 0.27
$api['turbopreise']['max_leverage_no_st']=0.27; //do not exceed this ratio, was 0.27

$api['turbopreise']['max_clicks_per_day_for_stuffing_shops']=10; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['turbopreise']['min_days_to_decide_stuffing_or_leveraging']=3; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed

//------------ turbopreise_s24 ------------------------------------------------------------
$api['turbopreise_s24']['api_base'] = "http://api.s24.com/v3";
$api['turbopreise_s24']['api_user'] = "8c2a214e";
$api['turbopreise_s24']['api_pw'] = "jknpcuhnnpeuiu9clmk5tcr457imixt5";
$api['turbopreise_s24']['user_key'] = "turbopreise_s24";
$api['turbopreise_s24']['mc'] = "pseudo_mc_s24_turbopreise";
$api['turbopreise_s24']['name'] = "s24";
$api['turbopreise_s24']['root'] = $all_root;
$api['turbopreise_s24']['web_root'] = $api['turbopreise_s24']['root'] . 'turbopreise/';
$api['turbopreise_s24']['click_db_path'] = $api['turbopreise_s24']['web_root'].'html/click/db/';
//$api['turbopreise_s24']['click_db_path'] = '/var/www/html/laravel/new.discountheld.de/html/click/db/';
$api['turbopreise_s24']['shop_list_path'] = $all_root . 'hub.noctemque.com/data/shoplist/turbopreise_s24_shoplist.json';
$api['turbopreise_s24']['base_url'] = 'https://turbopreise.de';

$api['turbopreise_s24']['leverage_ratio_st'] = 1.0;
//$api['turbopreise_s24']['leverage_ratio_st'] = 10;
//$api['turbopreise_s24']['leverage_ratio_st_pharmacies'] = 10;

$api['turbopreise_s24']['leverage_ratio_no_st'] = 1.0; // a-clicks divided by b-clicks
$api['turbopreise_s24']['max_leverage_no_st'] = 1.0; //do not exceed this ratio
//$api['turbopreise_s24']['leverage_ratio_no_st'] = 10; // a-clicks divided by b-clicks
//$api['turbopreise_s24']['max_leverage_no_st'] = 10; //do not exceed this ratio

$api['turbopreise_s24']['max_clicks_per_day_for_stuffing_shops'] = 10; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['turbopreise_s24']['min_days_to_decide_stuffing_or_leveraging'] = 5; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed


//------------ top-preistipps ------------------------------------------------------------
$api['top-preistipps']['api_base']="api.billiger.de/content/2.0";
$api['top-preistipps']['api_user']="toppreistipps_prod_API";
$api['top-preistipps']['api_pw']="FISmmlz4pB";
$api['top-preistipps']['user_key'] = "toppreistipps_prod_API";
$api['top-preistipps']['mc']="NfUedFTkvMMr";
$api['top-preistipps']['name']="top-preistipps";
$api['top-preistipps']['root']=$all_root.'top-preistipps.de/';
$api['top-preistipps']['web_root']=$api['top-preistipps']['root'].'html/';
$api['top-preistipps']['click_db_path']=$api['top-preistipps']['web_root'].'click/db/';
$api['top-preistipps']['shop_list_path'] = $all_root . 'hub.noctemque.com/data/shoplist/toppreistipps_prod_API_shoplist.json';
$api['top-preistipps']['base_url']='http://top-preistipps.de';

//$api['top-preistipps']['leverage_ratio_st']=0.63;
$api['top-preistipps']['leverage_ratio_st']=1.7;
$api['top-preistipps']['leverage_ratio_st_pharmacies']=20;

//$api['top-preistipps']['leverage_ratio_no_st']=0.40; // a-clicks divided by b-clicks
//$api['top-preistipps']['max_leverage_no_st']=0.40; //do not exceed this ratio
$api['top-preistipps']['leverage_ratio_no_st']=0.3; // a-clicks divided by b-clicks
$api['top-preistipps']['max_leverage_no_st']=0.3; //do not exceed this ratio

$api['top-preistipps']['max_clicks_per_day_for_stuffing_shops']=9; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['top-preistipps']['min_days_to_decide_stuffing_or_leveraging']=3; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed


//------------ preis-kenner ------------------------------------------------------------
$api['preis-kenner']['api_base']="api.billiger.de/content/2.0";
$api['preis-kenner']['api_user']="preiskenner_prod_API";
$api['preis-kenner']['api_pw']="gpFhR44FIy";
$api['preis-kenner']['user_key'] = "preiskenner_prod_API";
$api['preis-kenner']['mc']="lkQ9s7EMQi8K";
$api['preis-kenner']['name']="preis-kenner";
$api['preis-kenner']['root']=$all_root.'preis-kenner.de/';
$api['preis-kenner']['web_root']=$api['preis-kenner']['root'].'html/';
$api['preis-kenner']['click_db_path']=$api['preis-kenner']['web_root'].'click/db/';
$api['preis-kenner']['shop_list_path'] = $all_root . 'hub.noctemque.com/data/shoplist/preiskenner_prod_API_shoplist.json';
$api['preis-kenner']['base_url']='http://preis-kenner.de';

//$api['preis-kenner']['leverage_ratio_st']=1.2;
$api['preis-kenner']['leverage_ratio_st']=1.2;
$api['preis-kenner']['leverage_ratio_st_pharmacies']=20;

//$api['preis-kenner']['leverage_ratio_no_st']=1.3; // a-clicks divided by b-clicks
//$api['preis-kenner']['max_leverage_no_st']=1.3; //do not exceed this ratio
$api['preis-kenner']['leverage_ratio_no_st']=0.3; // a-clicks divided by b-clicks
$api['preis-kenner']['max_leverage_no_st']=0.3; //do not exceed this ratio

$api['preis-kenner']['max_clicks_per_day_for_stuffing_shops']=9; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['preis-kenner']['min_days_to_decide_stuffing_or_leveraging']=3; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed








//------------ noctemque_fr_prod ------------------------------------------------------------http://discounthero.org/fr/
$api['noctemque_fr_prod']['api_base']="api.solutenetwork.com";
$api['noctemque_fr_prod']['api_user']="noctemque_fr_prod";
$api['noctemque_fr_prod']['api_pw']="yVHg1DQZhs";
$api['noctemque_fr_prod']['user_key'] = "noctemque_fr_prod";
$api['noctemque_fr_prod']['mc']="noctemque_fr_prod";
$api['noctemque_fr_prod']['name']="noctemque_fr_prod";
$api['noctemque_fr_prod']['root']='/usr/home/ejdqpb/public_html/discounthero/fr/';
$api['noctemque_fr_prod']['web_root']=$api['noctemque_fr_prod']['root'].'html/';
$api['noctemque_fr_prod']['click_db_path']=$api['noctemque_fr_prod']['web_root'].'click/db/';
$api['noctemque_fr_prod']['shop_list_path'] = $all_root . 'hub.noctemque.com/data/shoplist/noctemque_fr_prod_shoplist.json';
$api['noctemque_fr_prod']['base_url']='http://discounthero.org/fr/';

$api['noctemque_fr_prod']['leverage_ratio_st']=1.5;
$api['noctemque_fr_prod']['leverage_ratio_st_pharmacies']=10;

$api['noctemque_fr_prod']['leverage_ratio_no_st']=0.24; // a-clicks divided by b-clicks
$api['noctemque_fr_prod']['max_leverage_no_st']=0.2; //do not exceed this ratio

$api['noctemque_fr_prod']['max_clicks_per_day_for_stuffing_shops']=10; //shops which get less than this ammount of clicks per day are not leveraged, but stuffed like shops without any a-clicks
$api['noctemque_fr_prod']['min_days_to_decide_stuffing_or_leveraging']=5; //after these number of days we decide if a shop with a-clicks, gets leveraded or stuffed


//paths - some are not used anymore
$portal_root_path["discountheld"]= __dir__ ."/../../discountheld/";
$portal_root_path["preispudel"]= __dir__ ."/../../discountheld/"; //	Exception!
$portal_root_path["turbopreise"]= __dir__ ."/../../turbopreise/";
$portal_root_path["noctemque_fr_prod"]= "/usr/home/ejdqpb/public_html/discounthero/fr/";

$click_db_path['discountheld']=$portal_root_path["discountheld"]."html/click/db/".date('Y-m-d').".db";
$click_db_path['preispudel']=$portal_root_path["preispudel"]."html/click/db/".date('Y-m-d').".db";
$click_db_path['turbopreise']=$portal_root_path["turbopreise"]."html/click/db/".date('Y-m-d').".db";
$click_db_path['noctemque_fr_prod']=$portal_root_path["noctemque_fr_prod"]."html/click/db/".date('Y-m-d').".db";

$acceptance_db_path = $all_root.'hub.noctemque.com/data/db/acceptance_list.db';